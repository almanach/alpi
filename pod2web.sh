#!/bin/sh

# generate xml web page in alpage web site from pod

ALPAGE_DIR=~/sites/site_alpage/

./pod2xml.pl | perl -p -e 's# xmlns="http://axkit.org/ns/2000/pod2xml"##' | \
xsltproc -o ${ALPAGE_DIR}/alpidoc.xml pod2xml.xsl -
