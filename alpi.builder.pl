#!/usr/bin/env perl
use strict;

=for developpers

Inspired from cpanminus. This scipt may be used to build a packed version alpi from alpi.pl

The file to update is now alpi.PL

=cut

open my $in,  "<", "alpi.pl" or die $!;
open my $out, ">", "alpi.tmp" or die $!;

print STDERR "Generating alpi from alpi.pl\n";

while (<$in>) {
    next if /Auto-removed/;
    s/DEVELOPERS:.*/DO NOT EDIT -- this is an auto generated file/;
    s/.*__FATPACK__/zomg_hfs(scalar `$^X -e "use App::FatPacker -run_script" file`)/e;
    print $out $_;
}

close $out;

unlink "alpi";
rename "alpi.tmp", "alpi";
chmod 0755, "alpi";

END { unlink "alpi.tmp" }

# HFS is case sensitive, we have fatlib/version that contains Version/Requirements.pm :(
sub zomg_hfs {
    local $_ = shift;
    s!version/Requirements.pm!Version/Requirements.pm!;
    s!fatpacked\{"lib/!fatpacked{"!og;
    return $_;
}
