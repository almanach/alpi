#!/usr/bin/env perl

# DEVELOPERS: Read script/build.PL in the repo how to update this
# __FATPACK__
use strict;

## ----------------------------------------------------------------------
## true start of alpi

use AppConfig qw/:argcount :expand/;
use File::Basename;
use Pod::Usage;

our $VERSION = '3.1.0';

# read configuration file / parse command line arguments
my $config = AppConfig->new(
                "verbose!"          => {DEFAULT => 1},
                "prefix=f"          => {DEFAULT => "$ENV{HOME}/exportbuild"},
                "aclocal=f"         => {DEFAULT => "aclocal-1.10"},
                "archive=f"         => {DEFAULT => "frmgchain"},
                "package|pkg=s@"    => {DEFAULT => []},
                "skippkg=s@"        => {DEFAULT => [qw{biomg alpino morph_it hmg MElt dyalog-sr dyalog-mst DepXML corpus_proc frmgdata}]}, # tmp forget about biomg
                "skipdep!"          => {DEFAULT => 0},
                "vardir=f"          => {DEFAULT => ''},
                "run!"              => {DEFAULT => 1},
                "linux32!"          => {DEFAULT => 0,
                            ACTION => sub {$_[2] and warn("*** the -linux32 option is deprecated")}
                           },
                "port|p=s"          => {DEFAULT => 8999},
                "user|u=s"          => {DEFAULT => ''},
                "group|g=s"         => {DEFAULT => ''},
                "gf|svn=s"          => {DEFAULT => 'anonymous'},
                "anon_mode_https!"       => {DEFAULT => 1},
                "ftp!"              => {DEFAULT => 0,
                            ACTION => sub {$_[2] and warn("*** installation via ftp is currently discouraged. Please use -svn option instead\n\n")}
                           },
                "force|f!"          => {DEFAULT => 0},
                "dependencies|dep!" => {DEFAULT => 0},
                "version|v!"        => {DEFAULT => 0},
                "project!"          => {DEFAULT => 0},
                "makedist!"         => {DEFAULT => 0},
                "info|i!"           => {DEFAULT => 0},
                "log=s"             => {DEFAULT => 'alpi.log'},
                "test!"             => {DEFAULT => 0},
                            "srcdir!"           => {DEFAULT => ''},
                    "date|d=s",
                            "biotim!"           => {DEFAULT => 0},
                "searchdirs=f@"     => {DEFAULT => ['/usr/local','/usr']},
                "aclocaldirs=f@"     => {DEFAULT => []},
                "update!" => {DEFAULT => 1},
                "expat_prefix=f",
                "use_install_base!" => {DEFAULT => 1},
                "parserd!" => {DEFAULT => 0},
                "help|usage!" => { DEFAULT => 0,
                           ACTION => sub {$_[2] and pod2usage({-exitval => 1,
                                           -verbose => 2,
#                                           -sections => [qw{DESCRIPTION USAGE OPTIONS}]
                                          })}
                         }
                           );

my $conffile = 'alpi.conf';
my @SAVEDARGS=@ARGV;

if (@ARGV && $ARGV[0] =~ m/^--config/) {
    shift @ARGV;
    $conffile = shift @ARGV;
    print STDERR "USING config file $conffile\n";
}

if (-r $conffile) {
    $config->file("$conffile")
    || die "can't open or process configuration file $conffile";
}

$config->args();

## check a few options
# test if 64b
##if (!$config->linux32) {
##    my $bit = `uname -m`;
##    if ($bit =~ m/64/) {
##       die "Please activate option linux32.\n";
##    }
##}

my $bit=`uname -m`;
my $is_64 = ($bit =~ m/64/) ? 1 : 0;

if ($config->user eq '') {
    my $user = `echo \$USER`;
    chomp $user;
    $config->set('user', $user);
}
elsif ($config->user eq 'root'){
    print "This script should be run locally, not as root...";
    exit;
}
if ($config->group eq '') {
    $config->set('group', (`groups` =~ m/^(\w+)/));
}

my $prefix      = $config->prefix;
my $srcdir      = $config->srcdir || "$prefix/src";
my $vardir      = $config->vardir || "$prefix/var";
my $log         = $config->log;
my $perlversion = sprintf("%vd",$^V);
my $gitversion = `git --version`;
my $atollftp    = "ftp://ftp.inria.fr/INRIA/Projects/Atoll/Eric.Clergerie";
my $almanach_gitlab = "gitlab.inria.fr/almanach";
my $alpagesvn    = "scm.gforge.inria.fr/svn";
my %LENV;
my $setenv_script;
my $parserd_setenv_script;
my $install_base = $config->use_install_base ? "INSTALL_BASE" : "PREFIX";

initenv();

print STDERR "prefix=$prefix\n";


# open log file
open(LOG,">$log") or die "can't open $log: $!";
my $date = `date`;
my $info = `uname -a`;
chomp $date;
chomp $info;
verbose(<<EOF);
Running $0 @SAVEDARGS
$date
$info
rev: \$Rev$ (\$LastChangedDate$)
----------------------------------
EOF

############################################
# Purpose    : print information for the user and write it in log file

sub verbose {
  return unless $config->verbose;
  print STDERR @_, "\n";
  print LOG @_,"\n";
}

#define objects types (defines the way they will be compiled)
my $autotools = 'Autotools';
my $perl      = 'Perl';
my $conda     = 'conda';

my $lingwb    = 'ALPAGE Linguistic Workbench';
my $mgkit     = 'Metagrammar Toolkit';


Packg::register( { NAME        => 'pkg-config',
                   DESC        => 'For querying installed libraries',
                   NONLOCALDEP => 1,
                   TYPE        => $autotools,
                   FTP         => 'http://pkgconfig.freedesktop.org/releases/pkg-config-0.29.2.tar.gz',
                   INSTALLED   => ['bin/pkg-config'],
                   VERSION     => ["0.29.2", "pkg-config", "--version"],
                   CONFIGURE => [qw{--with-internal-glib}],
                   AUTORECONF => 1,
                  });

# create and register ALPAGE packages:

# project DyALog
#################################################################

Packg::register( { NAME        => 'DyALog',
           DESC        => 'a parser compiler and logic programming environment',
           TYPE        => $autotools,
           GIT         => "http://gitlab.inria.fr/almanach/dyalog/dyalog.git", #-/archive/master/dyalog-master.tar.gz",
           FTP         => "http://gforge.inria.fr/frs/download.php/26413/DyALog-1.13.0.tar.gz",
           SVN         => "$alpagesvn/dyalog/DyALog/trunk",
           CONFIGURE   => ["--includedir=/usr/lib"],
           PROJECT     => 'dyalog',
           DEPEND      => { 'gc'      => 1,
                    'pkg-config' => 1,
                    'flex' => 1,
                    'bison' => 1,
                    'texinfo' => 1
                  },
           VERSION     => ["1.13.0", "dyalog-config", "--version"],
           AUTORECONF => 1,
           NEEDLIBTOOL => 1,
           });

Packg::register( { NAME        => 'dyalog-xml',
           DESC        => 'A DyALog module to access LibXML',
           TYPE        => $autotools,
           GIT         => "http://gitlab.inria.fr/almanach/dyalog/dyalog-xml.git", #-/archive/master/dyalog-xml-master.tar.gz",
           FTP         => "http://gforge.inria.fr/frs/download.php/26413/DyALog-1.13.0.tar.gz",
           SVN         => "$alpagesvn/dyalog/DyALog/trunk",
           PROJECT     => 'dyalog',
           DEPEND      => { DyALog       => 1,
                            libxml2      => 1,
                            'pkg-config' => 1,
                        },
           VERSION     => ["1.1.0", "dyalog-config", "--pkg=dyalog-xml --version"],
           AUTORECONF => 1,
           });

Packg::register( { NAME        => 'dyalog-sqlite',
           DESC        => 'A DyALog module to access SQLITE3 databases',
           TYPE        => $autotools,
           GIT         => "http://gitlab.inria.fr/almanach/dyalog/dyalog-sqlite.git", #-/archive/master/dyalog-sqlite-master.tar.gz",
           FTP         => "http://gforge.inria.fr/frs/download.php/26432/dyalog-sqlite-1.1.0.tar.gz",
           SVN         => "$alpagesvn/dyalog/dyalog-sqlite/trunk",
           PROJECT     => 'dyalog',
           DEPEND      => { DyALog       => 1,
                    'sqlite'     => 1,
                    'pkg-config' => 1,
                },
           VERSION     => ["1.1.0", "dyalog-config", "--pkg=dyalog-sqlite --version"],
           AUTORECONF => 1,
           });


Packg::register( { NAME        => 'dyalog-sr',
           DESC        => 'a transition-based parser on top of DyALog',
           TYPE        => $autotools,
           FTP         => "http://gforge.inria.fr/frs/download.php/26432/dyalog-sqlite-1.1.0.tar.gz",
           SVN         => "$alpagesvn/dyalog/dyalog-sr/trunk",
           GIT         => "http://gitlab.inria.fr/almanach/dyalog/dyalog-sr.git", #-/archive/master/dyalog-sr-master.tar.gz",
           PROJECT     => 'dyalog',
           DEPEND      => { DyALog       => 1,
                    'pkg-config' => 1,
                },
           VERSION     => ["1.0.0", "pkg-config", "--modversion dyalog-sr"],
           AUTORECONF => 1,
           NEEDLIBTOOL => 1,
           });

Packg::register( { NAME        => 'dyalog-mst',
           DESC        => 'a maximum spanning tree (MST) parser on top of DyALog',
           TYPE        => $autotools,
           FTP         => "http://gforge.inria.fr/frs/download.php/26432/dyalog-sqlite-1.1.0.tar.gz",
           SVN         => "$alpagesvn/dyalog/dyalog-mst/trunk",
           GIT         => "http://gitlab.inria.fr/almanach/dyalog/dyalog-mst.git", #-/archive/master/dyalog-mst-master.tar.gz",
           PROJECT     => 'dyalog',
           DEPEND      => { DyALog       => 1,
                    'pkg-config' => 1,
                },
           VERSION     => ["0.0.1", "pkg-config", "--modversion dyalog-mst"],
           AUTORECONF => 1,
           NEEDLIBTOOL => 1,
         });

Packg::register( { NAME        => 'corpus_proc',
           DESC        => 'various scripts for corpus processing',
           TYPE        => $autotools,
           SVN         => "$alpagesvn/lingwb/corpus_proc/trunk",
           GIT         => "http://gitlab.inria.fr/almanach/lingwb/corpus_proc.git", #-/archive/master/corpus_proc-master.tar.gz",
           PROJECT     => $lingwb,
           NOAUTORECONF => 1,
           NOCONFIGURE => 1,
           NOMAKE => 1,
           VERSION     => [],
           });

Packg::register( { NAME        => 'frmgdata',
           DESC        => 'big resources used for better performances of FRMG',
           TYPE        => $autotools,
           FTP         => "http://alpage.inria.fr/~clerger/frmgdata-0.0.1.tar.gz",
           GIT         => "http://gitlab.inria.fr/almanach/mgkit/frmg.git", #-/archive/master/frmg-master.tar.gz",
           PROJECT     => $mgkit,
           DEPEND      => { 'mgtools'       => 1,
                               'dyalog-xml'    => 1, 
                               'dyalog-sqlite'    => 1, 
                },
           AUTORECONF => 1,
           });


# project ALPAGE Linguistic Workbench
#################################################################

Packg::register( { NAME        => 'sxpipe',
           DESC        => 'a pre-parsing chain for French including segmentation, spelling corrections, lexicon lookup, named entities detections...',
           TYPE        => $autotools,
#           FTP         => 'http://gforge.inria.fr/frs/download.php/26649/sxpipe-2.1.tar.gz',
           FTP         => 'ftp://ftp.inria.fr/INRIA/Projects/Alpage/sxpipe-2.1.tar.gz',
           GIT            => "https://gitlab.inria.fr/almanach/alTextProcessing/sxpipe.git", #-/archive/master/sxpipe-master.tar.gz",
           SVN         => "$alpagesvn/lingwb/sxpipe/trunk",
           PROJECT     => $lingwb,
           AUTORECONF => 1,
           DEPEND      => { 'syntax'     => 1,
                    'lefff'      => 1,
                    'pkg-config' => 1,
                    'DBD-SQLite' => 1,
                    'aleda' => 1
                  },
           CONFIGURE   => [ # "--with-lefffdir=$LENV{SHAREDIR}/lefff",
                   "--with-syntaxlib=$LENV{LIBDIR}/syntax/libsx.a",
                   "--with-syntaxlibdir=$LENV{LIBDIR}/syntax",
                   "--with-syntaxincl=$LENV{INCLUDEDIR}/syntax",
                   "--with-syntaxbin=$LENV{BINDIR}",
                   "--with-syntaxsrc=$LENV{SHAREDIR}/syntax",
#                   "--with-alexinadir=$LENV{SHAREDIR}/alexina",
#                   "--with-alexinatoolsdir=$LENV{SHAREDIR}/alexina-tools",
                   # "--with-aledalibdir=$LENV{LIBDIR}/aleda",
                  ],
           VERSION     => ["2.1", "pkg-config", "--modversion sxpipe"],
           });

Packg::register( { NAME        => 'Lingua-Features',
           DESC        => 'Natural languages features',
           TYPE        => $perl,
           FTP         => "$atollftp/TAG/Lingua-Features-0.3.1.tar.gz",
           SVN         => "$alpagesvn/lingwb/Lingua-Features/trunk",
           GIT            => "https://gitlab.inria.fr/almanach/alOldies/perlmod/lingua-features.git", #-/archive/master/lingua-features-master.tar.gz",
           PROJECT     => $lingwb,
           DEPEND      => {'XML-Generator' => 1,
                   'Tie-IxHash'    => 1,
                   'List-Compare'  => 1, },
           VERSION     => ["0.3", "Lingua::Features"],
           });

Packg::register( { NAME        => 'Lingua-Matcher',
           DESC        => 'Data-based named entity matcher',
           TYPE        => $perl,
           FTP         => "$atollftp/TAG/Lingua-Matcher-0.1.tar.gz",
           SVN         => "$alpagesvn/lingwb/Lingua-Matcher/trunk",
           GIT            => "https://gitlab.inria.fr/almanach/alOldies/perlmod/lingua-matcher.git", #-/archive/master/lingua-matcher-master.tar.gz",
           PROJECT     => $lingwb,
           #CPAN        => ['Regexp::Assemble'],
           INSTALLED   => ["lib/perl5/site_perl/$perlversion/Lingua/Matcher.pm"],
           #VERSION     => ["0.1", "Lingua::Matcher"],
           });

Packg::register( { NAME        => 'forest_utils',
           DESC        => 'Perl conversion scripts for Shared Derivation Forests',
           TYPE        => $perl,
           FTP         => "http://gforge.inria.fr/frs/download.php/26423/forest_utils-0.2.tar.gz",
           GIT            => "https://gitlab.inria.fr/almanach/mgkit/forest_utils.git", #-/archive/master/forest_utils-master.tar.gz",
           SVN         => "$alpagesvn/lingwb/forest_utils/trunk",
           PROJECT     => $lingwb,
           DEPEND      => { 'XML-Parser'       => 1,
                    'XML-Generator'    => 1,
                    'Data-Grove'       => 1,
                    'Data-Dumper'      => 1
                    },
           VERSION     => ["0.2", "Forest"],
           });

Packg::register( { NAME        => 'parserd',
           DESC        => 'A server of parsers with clients',
           TYPE        => $autotools,
           FTP         => "http://gforge.inria.fr/frs/download.php/26422/parserd-2.3.0.tar.gz",
           SVN         => "$alpagesvn/lingwb/parserd/trunk",
           GIT            => "https://gitlab.inria.fr/almanach/mgkit/parserd.git", #-/archive/master/parserd-master.tar.gz",
           PROJECT     => $lingwb,
           AUTORECONF => 1,
           DEPEND      => {'forest_utils'      => 1 ,
                   'IO-Socket'         => 1,
                   'File-Temp'         => 1,
                   'Net-Server'        => 1,
                   'Net-Telnet'        => 1,
                   'IO-All'            => 1,
                   'IO-Compress-Bzip2' => 1,
                   'Term-Report'       => 1,
                   'Time-HiRes'        => 1,
                   'pkg-config'        => 1,
                    },
           POSTPROCESS => { update_conf    => 1 },
           CONFIGURE   => ["--with-modperldir=$LENV{MODPERLDIR}",
                   "--with-cgidir=$LENV{CGIDIR}",
                   "--with-initdir=$LENV{INITDIR}",
                   "--localstatedir=$LENV{VAR}"],
           VERSION     => ["2.3.0", "pkg-config", "--modversion parserd"], 
           });

Packg::register( { NAME        => 'MElt',
           DESC        => 'a tagger/lemmatizer',
           TYPE        => $autotools,
           FTP         => 'https://gforge.inria.fr/frs/download.php/file/33721/melt-2.0b5.tar.gz',
           SVN         => "$alpagesvn/lingwb/MElt/trunk",
           GIT            => "https://gitlab.inria.fr/almanach/alTextProcessing/melt.git", #-/archive/master/melt-master.tar.gz",
           PROJECT     => $lingwb,
           DEPEND      => { # 'syntax'     => 1,
                    # 'lefff'      => 1,
                    # 'pkg-config' => 1,
                    'DBD-SQLite' => 1,
                  },
           CONFIGURE   => [ # "--with-lefffdir=$LENV{SHAREDIR}/lefff",
                   #"--with-syntaxlib=$LENV{LIBDIR}/syntax/libsx.a",
                   #"--with-syntaxlibdir=$LENV{LIBDIR}/syntax",
                   #"--with-syntaxincl=$LENV{INCLUDEDIR}/syntax",
                   #"--with-syntaxbin=$LENV{BINDIR}",
                   #"--with-syntaxsrc=$LENV{SHAREDIR}/syntax",
                   #"--with-alexinadir=$LENV{SHAREDIR}/alexina",
                   # "--with-aledalibdir=$LENV{LIBDIR}/aleda",
                  ],
           VERSION     => ["2.0b5"],
           });


# project Metagrammar Toolkit
#################################################################

Packg::register( { NAME        => 'mgcomp',
           DESC        => 'A Meta-Grammar compiler written in DyALog',
           TYPE        => $autotools,
           FTP         => "http://gforge.inria.fr/frs/download.php/26420/mgcomp-1.5.0.tar.gz",
           SVN         => "$alpagesvn/mgkit/mgcomp/trunk",
           GIT            => "https://gitlab.inria.fr/almanach/mgkit/mgcomp.git", #-/archive/master/mgcomp-master.tar.gz",
           PROJECT     => $mgkit,
           AUTORECONF => 1,
           DEPEND      => { DyALog       => 1,
                    'pkg-config' => 1,},
           VERSION     => ["1.5.0", "pkg-config", "--modversion mgcomp"],
           });

Packg::register( { NAME        => 'mgtools',
           DESC        => 'An enviromnent to edit and view Meta-Grammars',
           TYPE        => $autotools,
           GIT         => "https://gitlab.inria.fr/almanach/mgkit/mgtools.git", #-/archive/master/mgtools-master.tar.gz",
           SVN         => "$alpagesvn/mgkit/mgtools/trunk",
           PROJECT     => $mgkit,
           DEPEND      => { libxml2      => 1 ,
                    	'XML-LibXML' => 1,
                    	'Event'      => 1,
                    	'pkg-config' => 1,
                    	flex => 1,
                    	bison => 1
                  },
           AUTORECONF => 1,
           VERSION     => ["2.2.2", "pkg-config", "--modversion mgtools"],
           });

Packg::register( { NAME        => 'frmg',
           DESC        => 'A French Meta Grammar',
           TYPE        => $autotools,
           FTP         => "http://gforge.inria.fr/frs/download.php/26655/frmg-2.0.1.tar.gz",
           SVN         => "$alpagesvn/mgkit/frmg/trunk",
           #GIT         => "https://gitlab.inria.fr/almanach/mgkit/frmg/-/archive/Release-2-0-0/frmg-Release-2-0-0.tar.gz",
           GIT         => "https://gitlab.inria.fr/almanach/mgkit/frmg.git", #-/archive/master/frmg-master.tar.gz",
           PROJECT     => $mgkit,
           AUTORECONF => 1,
           NEEDLIBTOOL => 1,
           CONFIGURE   => ["--with-modperldir=$LENV{MODPERLDIR}",
                   "--sysconfdir=$LENV{ETC}"],
            DEPEND      => { DyALog             => 1,
                    'dyalog-xml'       => 1,
                    'dyalog-sqlite'    => 1,
#                    mgcomp             => 1,
#                    mgtools            => 1,
                    'lefff-frmg'       => 1,
                    'tag_utils'        => 1,
                    'parserd'          => 1,
                    'libxslt'          => 1,
                    'Lingua-Features'  => 1,
                    'Data-Compare'     => 1,
                    'Parse-Eyapp'      => 1,
                    'pkg-config'       => 1,
                    'IPC-Run'          => 1,
                    'Text-Unaccent-PurePerl' => 1,
                    'sxpipe' => 1,
                   },
           SVNDEPEND => {
                 mgcomp => 1,
                 mgtools => 1,
                },
           VERSION     => ["2.0.0", "pkg-config", "--modversion frmg"],
           });


Packg::register( { NAME        => 'biomg',
           DESC        => 'A French Meta Grammar for botanical descriptions',
           TYPE        => $autotools,
           FTP         => "http://alpage.inria.fr/~cabrera/download/biomg-0.0.1.tar.gz",
           SVN         => "$alpagesvn/mgkit/biomg/trunk",
           GIT         => "https://gitlab.inria.fr/almanach/mgkit/biomg.git", #-/archive/master/biomg-master.tar.gz",
           PROJECT     => $mgkit,
           CONFIGURE   => ["--with-modperldir=$LENV{MODPERLDIR}",
                   "--sysconfdir=$LENV{ETC}"],
           AUTORECONF => 1,
           DEPEND      => { DyALog            => 1,
                    'dyalog-xml'      => 1,
                    'dyalog-sqlite'   => 1,
                    mgcomp            => 1,
                    mgtools           => 1,
                    'lefff-frmg'      => 1,
                    'tag_utils'       => 1,
                    'parserd'         => 1,
                    'libxslt'         => 1,
                    'Lingua-Features' => 1,
                    'Lingua-Matcher'  => 1,
                    'Data-Compare'    => 1,
                    'pkg-config'      => 1,
                   },
           VERSION     => ["0.0.1", "pkg-config", "--modversion biomg"],
           });


Packg::register( { NAME        => 'hmg',
           DESC        => 'A Greek Meta Grammar',
           TYPE        => $autotools,
           FTP         => "http://gforge.inria.fr/frs/download.php/26655/frmg-2.0.1.tar.gz",
           SVN         => "$alpagesvn/mgkit/hmg/trunk",
           GIT         => "https://gitlab.inria.fr/almanach/mgkit/hmg.git", #-/archive/master/hmg-master.tar.gz",
           PROJECT     => $mgkit,
           AUTORECONF => 1,
           CONFIGURE   => ["--with-modperldir=$LENV{MODPERLDIR}",
                   "--sysconfdir=$LENV{ETC}"],
            DEPEND      => { DyALog             => 1,
                    'dyalog-xml'       => 1,
                    'dyalog-sqlite'    => 1,
#                    mgcomp             => 1,
#                    mgtools            => 1,
#                    'lefff-frmg'       => 1,
                    'tag_utils'        => 1,
                    'parserd'          => 1,
                    'libxslt'          => 1,
                    'Lingua-Features'  => 1,
                    'Data-Compare'     => 1,
                    'Parse-Eyapp'      => 1,
                    'pkg-config'       => 1,
                    'IPC-Run'          => 1
                   },
           SVNDEPEND => {
                 mgcomp => 1,
                 mgtools => 1,
                },
           VERSION     => ["0.0.1", "pkg-config", "--modversion hmg"],
           });


Packg::register( { NAME        => 'tag_utils',
           DESC        => 'Perl conversion scripts for Tree Adjoining Grammars',
           TYPE        => $perl,
           FTP         => "http://gforge.inria.fr/frs/download.php/26433/tag_utils-1.13.tar.gz",
           SVN         => "$alpagesvn/mgkit/tag_utils/trunk",
           GIT         => "https://gitlab.inria.fr/almanach/mgkit/tag-utils.git", #-/archive/master/tag-utils-master.tar.gz",
           PROJECT     => $mgkit,
           DEPEND        => { 
                    'XML-Parser'     => 1,
                    'DBI'            => 1, 
                    'XML-Generator'  => 1,
                    'Parse-Yapp'     => 1,
                    'Data-Grove'     => 1,
                     AppConfig => 1
                    },
           VERSION     => ['1.13', 'TAG'],
           });

Packg::register( { NAME        => 'DepXML',
           DESC        => 'Perl module implementing DPath query language over DepXML structures',
           TYPE        => $perl,
           SVN         => "$alpagesvn/lingwb/corpus_proc/trunk/DepXML",
           #GIT         => "https://gitlab.inria.fr/almanach/alCorpusManagement/corpus_proc/-/archive/master/corpus_proc-master.tar.gz", #it is not possible to only download a subfolder from git at the moment as a targz
           PROJECT     => $mgkit,
           DEPEND        => { 
                    'XML-Twig'     => 1,
                    'Devel-Declare'   => 1, 
                    'B-Hooks-EndOfScope'  => 1,
                    'Scalar-Util'     => 1,
                    },
           VERSION     => [],
           });


# project Alexina
#################################################################

Packg::register( { NAME        => 'alexina-tools',
           TYPE        => $autotools,
           FTP         => "http://gforge.inria.fr/frs/download.php/26425/alexina-tools-1.3.2.tar.gz",
           GIT         => "https://gitlab.inria.fr/almanach/alexina/alexina-tools.git", #-/archive/master/alexina-tools-master.tar.gz",
           SVN         => "$alpagesvn/alexina/alexina-tools/trunk",
##           PRIVATE     => 1,
           PROJECT     => 'Alexina',
           AUTORECONF => 1,
           #INSTALLED   => ["share/alexina-tools",]
               });

Packg::register( { NAME        => 'lefff',
           DESC        => 'A French Morphological and Syntactic Lexicon',
           TYPE        => $autotools,                      
           FTP         => "http://gforge.inria.fr/frs/download.php/26426/lefff-3.0.2.tar.gz",
           GIT         => "https://gitlab.inria.fr/almanach/alexina/lefff.git", #-/archive/master/lefff-master.tar.gz",
           SVN         => "$alpagesvn/alexina/lefff/trunk",
##           PRIVATE     => 1,
           CONFIGURE   => ["--with-alexinatoolsdir=$LENV{SHAREDIR}/alexina-tools",
                   "--enable-rawscripts"],
           PROJECT     => 'Alexina',
           AUTORECONF => 1,
           DEPEND      => { 'alexina-tools' => 1,
                    },
               });

Packg::register( { NAME        => 'lefff-frmg',
           DESC        => 'Adaptation of Lefff for FRMG',
           TYPE        =>  $autotools,
           FTP         => "http://gforge.inria.fr/frs/download.php/26424/lefff-frmg-0.4.tar.gz",
           GIT         => "https://gitlab.inria.fr/almanach/alexina/lefff-frmg.git", #-/archive/master/lefff-frmg-master.tar.gz",
           SVN         => "$alpagesvn/alexina/lefff-frmg/trunk",
##           PRIVATE     => 1,
           CONFIGURE   => ["--with-lefffdir=$LENV{SHAREDIR}/lefff"],
           PROJECT     => 'Alexina',
           AUTORECONF => 1,
           DEPEND      => { lefff        => 1,
                    'pkg-config' => 1,
                  },
           VERSION     => ["0.4", "pkg-config", "--modversion lefff-frmg"],
               });


Packg::register( { NAME        => 'alpino',
           DESC        => 'A Dutch Morphological and Syntactic Lexicon',
           TYPE        => $autotools,     
           GIT         => "https://gitlab.inria.fr/almanach/alexina/alpino.git", #-/archive/master/alpino-master.tar.gz",
           SVN         => "$alpagesvn/alexina/alpino/trunk",
           CONFIGURE   => ["--with-alexinatoolsdir=$LENV{SHAREDIR}/alexina-tools",
                   "--enable-rawscripts"],
           PROJECT     => 'Alexina',
           DEPEND      => { 'alexina-tools' => 1,
                  },
         });

Packg::register( { NAME        => 'morph_it',
           DESC        => 'An Italian  Morphological and Syntactic Lexicon',
           TYPE        => $autotools,     
           GIT         => "https://gitlab.inria.fr/almanach/alexina/morph_it.git", #-/archive/master/morph_it-master.tar.gz",
           SVN         => "$alpagesvn/alexina/morph_it/trunk",
           CONFIGURE   => ["--with-alexinatoolsdir=$LENV{SHAREDIR}/alexina-tools",
                   "--enable-rawscripts"],
           PROJECT     => 'Alexina',
           DEPEND      => { 'alexina-tools' => 1,
                  },
         });


Packg::register( { NAME        => 'aleda',
           DESC        => 'named entity database for sxpipe',
           TYPE        => $autotools,
           FTP         => 'ftp://ftp.inria.fr/INRIA/Projects/Alpage/sxpipe-2.1.tar.gz',
           GIT         => "https://gitlab.inria.fr/almanach/alexina/aleda.git", #-/archive/master/aleda-master.tar.gz",
           SVN         => "$alpagesvn/alexina/aleda/trunk",
           PROJECT     => 'Alexina',
           DEPEND      => { 'alexina-tools' => 1, lefff => 1 },
           AUTORECONF => 1,
           CONFIGURE   => ["--with-alexinatoolsdir=$LENV{SHAREDIR}/alexina-tools",
                   "--with-alexinadir=$LENV{SHAREDIR}/alexina",
#                   "--with-lefffdir=$LENV{SHAREDIR}/lefff"
                  ],
##           VERSION     => ["2.1", "pkg-config", "--modversion sxpipe"],
         });


# SYNTAX
#################################################################

Packg::register( { NAME        => 'syntax',
           DESC        => "To compile tools that rely on SYNTAX",
           TYPE        => $autotools,
           FTP         => "http://gforge.inria.fr/frs/download.php/25190/syntax-6.0b7.tar.gz",
           #tSVN         => "$alpagesvn/syntax/trunk",
           PROJECT     => 'syntax',
            });


# non local dependencies
#################################################################

Packg::register( { NAME => 'texinfo',
           DESC => "documentation builder",
           TYPE => $autotools,
           NONLOCALDEP => 1,
           FTP => "http://ftp.gnu.org/gnu/texinfo/texinfo-6.3.tar.gz",
           VERSION => ["4.0","makeinfo","--version"]
         }
           );

Packg::register( { NAME        => 'flex',
           DESC        => "lexical analyzer",
           TYPE        => $autotools,
           NONLOCALDEP => 1,
           FTP         => "https://github.com/westes/flex/releases/download/v2.6.4/flex-2.6.4.tar.gz",
           VERSION     => ["2.5.4","flex","--version"],
           });

Packg::register( { NAME        => 'bison',
           DESC        => "lexical analyzer",
           TYPE        => $autotools,
           NONLOCALDEP => 1,
           FTP         => "http://ftp.gnu.org/gnu/bison/bison-3.0.4.tar.gz",
           VERSION     => ["2.3","bison","--version"],
           });

Packg::register( { NAME        => 'gc',
           DESC        => "Boehm\'s Garbage Collector",
           TYPE        => $autotools,
           NONLOCALDEP => 1,
           FTP         => "http://www.hboehm.info/gc/gc_source/gc-7.2f.tar.gz",
           INSTALLED   => ["include/gc/gc.h",
                   "lib/libgc.la",],
           CONFIGURE => ["--disable-threads","--enable-large-config"] ## some pb when no threads

           });

Packg::register( { NAME        => 'sqlite',
           DESC        => 'C library that implements an SQL database engine',
           NONLOCALDEP => 1,
           TYPE        => $autotools,
           FTP => 'https://sqlite.org/2013/sqlite-autoconf-3080002.tar.gz',
##           CONFIGURE   => ['--disable-tcl'],
            INSTALLED   => ["include/sqlite3.h",
                   "lib/libsqlite3.la",],
           #VERSION     => ["3.3.4", "sqlite3", "-version"],
           PKGCHECK => [sqlite3 => "3.0.0"]
           });

Packg::register( { NAME        => 'libxml2',
           DESC        => 'XML C parser and toolkit',
           NONLOCALDEP => 1,
           TYPE        => $autotools,
           FTP         => 'ftp://xmlsoft.org/libxml2/libxml2-2.9.2.tar.gz',
           INSTALLED   => ["include/libxml2/libxml/xmlversion.h",
#                   "lib/libxml2.la",
                   "share/aclocal/libxml.m4"
                  ],
           CONFIGURE => ['--without-python'],
           VERSION     => ["2.6.23", "xml2-config", "--version"],
           });

Packg::register( { NAME        => 'libxslt',
           DESC        => 'XSLT C library',
           NONLOCALDEP => 1,
           TYPE        => $autotools,
           FTP         => 'ftp://xmlsoft.org/libxslt/libxslt-1.1.28.tar.gz',
           CONFIGURE => ['--without-python'],
           INSTALLED   => ["include/libexslt/exslt.h"],
           VERSION     => ["1.1.9", "xslt-config", "--version"],
           });

Packg::register( { NAME        => 'graphviz',
           DESC        => 'Graph Visualization',
           NONLOCALDEP => 1,
           TYPE        => $autotools,
           FTP => 'https://graphviz.gitlab.io/pub/graphviz/stable/SOURCES/graphviz.tar.gz',
           CONFIGURE => ["--with-pnglibdir=/usr/local/lib/"], 
           INSTALLED   => ["bin/dot"],
           VERSION     => ['2.40.1', 'dot', '-V'],
           });

# To be activated (maybe)
# Packg::register({NAME => 'texinfo',
#          DESC => 'Texinfo - The GNU Documentation System',
#          NONLOCALDEP => 1,
#          TYPE => $autotools,
#          FTP => 'http://ftp.gnu.org/gnu/texinfo/texinfo-4.11.tar.gz',
#          INSTALLED => ["bin/makeinfo"]
#         }
#            );

# PERL dependencies
#################################################################

Packg::register( { NAME        => 'Data-Compare',
           DESC        => 'compare perl data structures',
           NONLOCALDEP => 1,
           TYPE        => $perl,
           DEPEND      => { 'File-Find-Rule' => 1 },
           FTP         => 'http://search.cpan.org/CPAN/authors/id/D/DC/DCANTRELL/Data-Compare-1.25.tar.gz',
           VERSION     => ["0.13", "Data::Compare"],
           });

Packg::register( { NAME        => 'File-Find-Rule',
           DESC        => 'compare perl data structures',
           NONLOCALDEP => 1,
           TYPE        => $perl,
           FTP         => 'http://search.cpan.org/CPAN/authors/id/R/RC/RCLAMP/File-Find-Rule-0.32.tar.gz',
           VERSION     => ["0.30", "File::Find::Rule"],
           });

Packg::register( { NAME        => 'DBI',
           DESC        => '',
           NONLOCALDEP => 1,           
           TYPE        => $perl,
           FTP         => 'http://search.cpan.org/CPAN/authors/id/T/TI/TIMB/DBI-1.640.tar.gz',
           VERSION     => ["1.615", "DBI"],
           });

Packg::register( { NAME      => 'DBD-SQLite',
             DESC      => 'driver DBD for SQLite',
             NONLOCALDEP => 1,
             TYPE      => $perl,
             FTP       => 'http://search.cpan.org/CPAN/authors/id/I/IS/ISHIGAKI/DBD-SQLite-1.54.tar.gz',
             VERSION   => ['1.31',"DBD::SQLite"],
             DEPEND => { 'sqlite' => 1 }
           }
         );

Packg::register( { NAME        => 'Data-Dumper',
           DESC        => '',
           NONLOCALDEP => 1,           
           TYPE        => $perl,
           FTP         => 'http://search.cpan.org/CPAN/authors/id/S/SM/SMUELLER/Data-Dumper-2.161.tar.gz',
           VERSION     => ["2.121", "Data::Dumper"],
           });
Packg::register( { NAME        => 'Data-Grove',
           DESC        => '',
           NONLOCALDEP => 1,           
           TYPE        => $perl,
           FTP         => 'http://search.cpan.org/CPAN/authors/id/K/KM/KMACLEOD/libxml-perl-0.08.tar.gz',
           VERSION     => ["0.08", "Data::Grove"],
           });
Packg::register( { NAME        => 'Event',
           DESC        => '',
           NONLOCALDEP => 1,           
           TYPE        => $perl,
           FTP         => 'http://search.cpan.org/CPAN/authors/id/J/JP/JPRIT/Event-1.18.tar.gz',
           VERSION     => ["1.08", "Event"],
           });
Packg::register( { NAME        => 'File-Temp',
           DESC        => '',
           NONLOCALDEP => 1,           
           TYPE        => $perl,
           FTP         => 'http://search.cpan.org/CPAN/authors/id/T/TJ/TJENNESS/File-Temp-0.22.tar.gz',
           VERSION     => ["0.16", "File::Temp"],
           });
Packg::register( { NAME        => 'IO-All',
           DESC        => '',
           NONLOCALDEP => 1,           
           TYPE        => $perl,
           FTP         => 'http://search.cpan.org/CPAN/authors/id/F/FR/FREW/IO-All-0.87.tar.gz',
           VERSION     => ["0.33", "IO::All"],
           });

Packg::register( { NAME        => 'IO-Pty',
           DESC        => '',
           NONLOCALDEP => 1,           
           TYPE        => $perl,
           FTP         => 'http://search.cpan.org/CPAN/authors/id/T/TO/TODDR/IO-Tty-1.12.tar.gz',
           VERSION     => ["1.07", "IO::Pty"],
           });

Packg::register( { NAME        => 'IO-Compress-Bzip2',
           DESC        => 'Write bzip2 files/buffers',
           NONLOCALDEP => 1,           
           TYPE        => $perl,
           FTP         => 'http://search.cpan.org/CPAN/authors/id/P/PM/PMQS/IO-Compress-2.034.tar.gz',
           DEPEND      => { 'Compress-Raw-Bzip2' => "2.031",
           },
           VERSION     => ["2.030", "IO::Compress::Bzip2"],
           });

Packg::register( { NAME        => 'Compress-Raw-Bzip2',
           DESC        => 'Low-Level Interface to bzip2 compression library',
           NONLOCALDEP => 1,           
           TYPE        => $perl,
           FTP         => 'http://search.cpan.org/CPAN/authors/id/P/PM/PMQS/Compress-Raw-Bzip2-2.034.tar.gz',
           VERSION     => ["2.031", "Compress::Raw::Bzip2"],
           });


Packg::register( { NAME        => 'IO-Socket',
           DESC        => '',
           NONLOCALDEP => 1,           
           TYPE        => $perl,
           FTP         => 'http://search.cpan.org/CPAN/authors/id/G/GB/GBARR/IO-1.25.tar.gz',
           VERSION     => ["1.25", "IO::Socket"],
           });

Packg::register( { NAME        => 'List-Compare',
           DESC        => '',
           NONLOCALDEP => 1,           
           TYPE        => $perl,
           FTP         => 'http://search.cpan.org/CPAN/authors/id/J/JK/JKEENAN/List-Compare-0.53.tar.gz',
           VERSION     => ["0.33", "List::Compare"],
           });

Packg::register( { NAME        => 'Net-Server',
           DESC        => '',
           NONLOCALDEP => 1,           
           TYPE        => $perl,
           FTP         => 'http://search.cpan.org/CPAN/authors/id/R/RH/RHANDOM/Net-Server-0.99.tar.gz',
           VERSION     => ["0.97", "Net::Server"],
           });
Packg::register( { NAME        => 'Net-Telnet',
           DESC        => '',
           NONLOCALDEP => 1,           
           TYPE        => $perl,
           FTP         => 'http://search.cpan.org/CPAN/authors/id/J/JR/JROGERS/Net-Telnet-3.03.tar.gz',
           VERSION     => ["3.03", "Net::Telnet"],
           });

Packg::register( { NAME        => 'Parse-Eyapp',
           DESC        => 'Extensions for Parse::Yapp',
           NONLOCALDEP => 1,           
           TYPE        => $perl,
           FTP => 'http://search.cpan.org/CPAN/authors/id/C/CA/CASIANO/Parse-Eyapp-1.181.tar.gz',
           DEPEND      => { 'version' => 1 },
           DEPEND      => { 'List-MoreUtils' => 1 },
           ## todo: find other way to check version
           VERSION     => ["1.147", "Parse::Eyapp"],
           });

Packg::register( { NAME        => 'List-MoreUtils',
           DESC        => 'Provide the stuff missing in List::Util',
           NONLOCALDEP => 1,           
           TYPE        => $perl,
           FTP         => 'http://search.cpan.org/CPAN/authors/id/A/AD/ADAMK/List-MoreUtils-0.30.tar.gz',
           VERSION     => ["0.22", "List::MoreUtils"],
           });

Packg::register( { NAME        => 'Parse-Yapp',
           DESC        => '',
           NONLOCALDEP => 1,           
           TYPE        => $perl,
           FTP         => 'http://search.cpan.org/CPAN/authors/id/F/FD/FDESAR/Parse-Yapp-1.05.tar.gz',
           ## todo: find other way to check version
           #VERSION     => ["0.38", "Parse::Yapp"],
           });

Packg::register( { NAME        => 'Term-Report',
           DESC        => '',
           NONLOCALDEP => 1,           
           TYPE        => $perl,
           FTP         => 'http://search.cpan.org/CPAN/authors/id/S/SH/SHAYH/Term-Report-1.18.tar.gz',
           VERSION     => ["1.18", "Term::Report"],
           });
Packg::register( { NAME        => 'Tie-IxHash',
           DESC        => '',
           NONLOCALDEP => 1,           
           TYPE        => $perl,
           FTP => 'http://search.cpan.org/CPAN/authors/id/C/CH/CHORNY/Tie-IxHash-1.22.tar.gz',
           ##  FTP         => 'http://search.cpan.org/CPAN/authors/id/G/GS/GSAR/Tie-IxHash-1.22.tar.gz',
           VERSION     => ["1.21", "Tie::IxHash"],
           });
Packg::register( { NAME        => 'Time-HiRes',
           DESC        => '',
           NONLOCALDEP => 1,           
           TYPE        => $perl,
           FTP         => 'http://search.cpan.org/CPAN/authors/id/J/JH/JHI/Time-HiRes-1.9721.tar.gz',
           VERSION     => ["1.86", "Time::HiRes"],
           });

Packg::register( { NAME        => 'version',
            DESC        => '',
            NONLOCALDEP => 1,           
            TYPE        => $perl,
            FTP         => 'http://search.cpan.org/CPAN/authors/id/J/JP/JPEACOCK/version-0.88.tar.gz',
            VERSION     => ["0.71", "version"],
            });

Packg::register( { NAME        => 'XML-Generator',
           DESC        => '',
           NONLOCALDEP => 1,           
           TYPE        => $perl,
           FTP         => 'http://search.cpan.org/CPAN/authors/id/B/BH/BHOLZMAN/XML-Generator-1.04.tar.gz',
           VERSION     => ["1.01", "XML::Generator"],
           });
Packg::register( { NAME        => 'XML-LibXML',
           DESC        => '',
           NONLOCALDEP => 1,           
           TYPE        => $perl,
           CONFIGURE   => [
                   'SKIP_SAX_INSTALL=1'
                  ],
           FTP         => 'http://search.cpan.org/CPAN/authors/id/P/PA/PAJAS/XML-LibXML-1.70.tar.gz',
           VERSION     => ["1.64", "XML::LibXML"],
           });
Packg::register( { NAME        => 'XML-Parser',
           DESC        => '',
           NONLOCALDEP => 1,           
           TYPE        => $perl,
           FTP         => 'http://search.cpan.org/CPAN/authors/id/C/CH/CHORNY/XML-Parser-2.40.tar.gz',
           VERSION     => ["2.34", "XML::Parser"],
           CONFIGURE =>  get_expat_params()
         });

Packg::register( { NAME        => 'AppConfig',
           DESC          => 'handling cmd options',
           NONLOCALDEP   => 1,           
           CONDA         => 'perl-appconfig'
           CONDA_CHANNEL => 'bioconda'
           TYPE          => $perl,
           FTP           => 'http://search.cpan.org/CPAN/authors/id/A/AB/ABW/AppConfig-1.66.tar.gz',
           VERSION       => ["1.66", "AppConfig"],
         });

Packg::register( { NAME        => 'IPC-Run',
           DESC        => '',
           NONLOCALDEP => 1,           
           TYPE        => $perl,
           FTP         => 'http://search.cpan.org/CPAN/authors/id/T/TO/TODDR/IPC-Run-0.96.tar.gz',
           VERSION     => ["0.82", "IPC::Run"],
         });

Packg::register( { NAME        => 'Text-Unaccent-PurePerl',
           DESC        => '',
           NONLOCALDEP => 1,           
           TYPE        => $perl,
           FTP         => 'http://search.cpan.org/CPAN/authors/id/P/PJ/PJACKLAM/Text-Unaccent-PurePerl-0.05.tar.gz',
           VERSION     => ["0.05", "Text::Unaccent::PurePerl"],
         });

Packg::register( { NAME        => 'XML-Twig',
           DESC        => '',
           NONLOCALDEP => 1,           
           TYPE        => $perl,
#           FTP         => 'http://search.cpan.org/CPAN/authors/id/P/PJ/PJACKLAM/Text-Unaccent-PurePerl-0.05.tar.gz',
#           VERSION     => ["0.05", "Text::Unaccent::PurePerl"],
         });

Packg::register( { NAME        => 'Devel-Declare',
           DESC        => '',
           NONLOCALDEP => 1,           
           TYPE        => $perl,
#           FTP         => 'http://search.cpan.org/CPAN/authors/id/P/PJ/PJACKLAM/Text-Unaccent-PurePerl-0.05.tar.gz',
#           VERSION     => ["0.05", "Text::Unaccent::PurePerl"],
         });

Packg::register( { NAME        => 'B-Hooks-EndOfScope',
           DESC        => '',
           NONLOCALDEP => 1,           
           TYPE        => $perl,
#           FTP         => 'http://search.cpan.org/CPAN/authors/id/P/PJ/PJACKLAM/Text-Unaccent-PurePerl-0.05.tar.gz',
#           VERSION     => ["0.05", "Text::Unaccent::PurePerl"],
         });

Packg::register( { NAME        => 'Scalar-Util',
           DESC        => '',
           NONLOCALDEP => 1,           
           TYPE        => $perl,
#           FTP         => 'http://search.cpan.org/CPAN/authors/id/P/PJ/PJACKLAM/Text-Unaccent-PurePerl-0.05.tar.gz',
#           VERSION     => ["0.05", "Text::Unaccent::PurePerl"],
         });

sub get_expat_params {
  ## check if expat parameters should be added to compile XML::Parser
  my $options = [];
  if ($ENV{EXPATINCPATH}) {
    push(@$options,"EXPATINCPATH=$ENV{EXPATINCPATH}");
  }
  if ($ENV{EXPATLIBPATH}) {
    push(@$options,"EXPATLIBPATH=$ENV{EXPATLIBPATH}");
  }
  if (!@$options && $config->expat_prefix) {
    my $path = $config->expat_prefix;
    $options = ["EXPATINCPATH=$path/include",
        "EXPATLIBPATH=$path/lib"
           ];
  }
  return $options;
}

# ALPI dependencies (if script is run by developper)
#################################################################


Packg::register( { NAME        => 'm4',
           ALPAGEDEP   => 1,
            NONLOCALDEP => 1,
           TYPE        => $autotools,
           FTP         => "http://ftp.gnu.org/gnu/m4/m4-latest.tar.gz",
            VERSION     => ["1.4", "m4", "--version"],
            });

Packg::register( { NAME        => 'autoconf',
           ALPAGEDEP   => 1,
            NONLOCALDEP => 1,
           TYPE        => $autotools,
           FTP         => "http://ftp.gnu.org/gnu/autoconf/autoconf-latest.tar.gz",
            VERSION     => ["2.60", "autoconf", "--version"],
           DEPEND      => { m4 => 1 }
            });

sub which_libtool {
  if ($^O =~ /darwin/) {
    ##    print "On Darwin archi\n";
    verbose "on Mac OS use glibtool and glibtoolize";
    $ENV{LIBTOOLIZE} = "glibtoolize";
    return ["1.5", "glibtool", "--version"];
  } else {
    return ["1.5", "libtool", "--version"];
  }
}

Packg::register( { NAME        => 'libtool',
           ALPAGEDEP   => 1,
            NONLOCALDEP => 1,
            TYPE        => $autotools,
            FTP         => "http://ftpmirror.gnu.org/libtool/libtool-2.4.6.tar.gz",
            VERSION     => which_libtool(),
         });

Packg::register( { NAME        => 'automake',
           ALPAGEDEP   => 1,
            NONLOCALDEP => 1,
           TYPE        => $autotools,
           FTP         => "http://ftp.gnu.org/gnu/automake/automake-1.11.2.tar.gz",
            VERSION     => ["1.10", "automake", "--version"],
           DEPEND      => { autoconf => 1 }
            });


#################################################################
## We start

my %required = ();
my @packages = @{$config->package};

##todo get revision numbers
if (@packages) {
  if ($config->parserd && !grep { $_ eq 'parserd' } @packages) {
    push(@packages,'parserd');
  }
  foreach my $pkg (@packages) {
    $required{$pkg} = 1;
  }
  foreach my $pkg (@packages) {
    if ((my ($name, $rev)) = $pkg =~ m/(\S*)[~-]r(\d*)$/ ) {
      # we can ask for a specific SVN revision with <pkg>~<rev>
      $pkg = $name;
      $Packg::package_list{$pkg}->{REVISION} = $rev;
      ## if user forgot to mention svn=anonymous
      $config->svn or $config->set('svn', 'anonymous');
    }
  }
}
@packages = sort grep {!exists $Packg::package_list{$_}{ALPAGEDEP}} keys(%Packg::package_list) unless (@packages);

if ($config->svn) {
  unshift @packages, grep {exists $Packg::package_list{$_}{ALPAGEDEP}} keys %Packg::package_list;
  print "package list is @packages\n";
}

if (@ARGV) {
  my $cmd = $ARGV[0];
  if ($cmd eq "check_path") {
    check_path();
  }
  exit;
}

sub check_path {
  ## for all non local Perl modules, check if the FTP path works
  foreach my $package (@packages) {
    my $pkg = $Packg::package_list{$package};
    next unless ($pkg->{FTP});
    $pkg->check_ftp_path;
  }
}

# create directories
mkdir "$prefix";
mkdir "$vardir";

chdir "$vardir";
foreach my $dir (qw{log run}) {
    mkdir $dir;
}

chdir "$prefix";
foreach my $dir (qw{share share/aclocal lib lib/perl5 bin sbin etc etc/init.d}) {
    mkdir $dir;
}
mkdir $srcdir;

my $modulerc = "$LENV{MODULEBUILDRC}";
unless (-f $modulerc) {
  mkdir "$prefix/lib/perl5";
  open(CONFIG,">$modulerc") || die "can't open $modulerc: $!";
  print CONFIG <<EOF;
## options for Module::Build
install  --install_base $prefix
EOF
  close(CONFIG);
}

my $cpanm="$prefix/bin/cpanm";

## install standalone cpan minus installer
unless (-f $cpanm && -X $cpanm) {
  system("wget --no-check-certificate -O $cpanm http://cpanmin.us")
##    || die "could not download cpanm"
      ;
  system("chmod +x $cpanm");
}


sub install_cpanm {
  ##  curl -L http://cpanmin.us | perl - App::cpanminus
  ##  wget -O -  http://cpanmin.us | perl - App::cpanminus
  ##  http://cpanmin.us | perl - App::cpanminus
}


($config->date && !$config->svn) and $config->set('svn', 'anonymous');
($config->svn eq '') or $config->set('ftp', 0);


# if machine is 64b and DyALog is installed, activate 32b emulation
# TODO
# if (!$config->linux32) {
#     my $bit = `uname -m`;
#     if ($bit =~ m/64/) {
#     foreach my $pkg (@packages) {
#         if ($pkg eq "DyALog") {
#         $config->linux32(1);
#         return;
#         }
#         else {
#         foreach
#         }
#     }
#     }
# }

if ($config->info) {
    foreach my $package (@packages) {
    my $pkg = $Packg::package_list{$package};
    if (exists $pkg->{DESC}) {
        print $pkg->{NAME}, "\t\t", $pkg->{DESC}, "\n";
    }
    }
    exit;
}

if ($config->makedist) {
    foreach my $package (@packages) {
    my $pkg = $Packg::package_list{$package};
    $pkg->makedist;
    }
    exit;
}


# print dependencies
if ($config->dependencies) {
    print_nonlocaldep(@packages);
    exit;
}

sub print_nonlocaldep {

    foreach my $package (@_) {
    my $pkg = $Packg::package_list{$package};
    
    next if ($pkg->{DEPCHECK});
    next if ($pkg->{ALPAGEDEP} && !$config->svn);

    print_nonlocaldep( keys %{$pkg->{DEPEND}} ) if $pkg->{DEPEND};
    print_nonlocaldep( keys %{$pkg->{SVNDEPEND}} ) if ($config->svn 
                               && $pkg->{SVNDEPEND});
    
    if ($pkg->{NONLOCALDEP} && !$pkg->uptodate()) {
        my $dep = $pkg->{NAME};
        $dep .= ' '.${$pkg->{VERSION}}[0] if $pkg->{VERSION};
        print $dep, "\n";
    }
    $pkg->{DEPCHECK} = 1;
    }

}

# print versions of ALPAGE packages
if ($config->version) {

    foreach my $package (@packages) {
    if(!exists $Packg::package_list{$package}) {
        verbose "Package '$package' does not exist.";
        die;
    }
    else {
        my $pkg = $Packg::package_list{$package};
        next if ($pkg->{NONLOCALDEP}); # only print versions of ALPAGE pkg
        if (exists $pkg->{VERSION}) {
        my $version = $pkg->version();
        verbose $pkg->{NAME}, " $version" if ($version) ;
        }
    }
    }
    exit;
}

if ($config->project) {

    foreach my $pkg_name (@packages) {
    my $pkg = $Packg::package_list{$pkg_name};
    print $pkg->{NAME}, " => ", $pkg->{PROJECT}, "\n"
        if (!$pkg->{NONLOCALDEP} && $pkg->{PROJECT});
    }
    exit;
}

############################################
# Purpose    : Check ALPAGE dependencies of this script

sub check_alpage_dep {
    my @packages = values (%Packg::package_list);
    my $message = '';
    foreach my $pkg (@packages) {
    if ($pkg->{ALPAGEDEP}) {
        $message .= "$pkg->{NAME} @{$pkg->{VERSION}}[0]\n" if (!$pkg->uptodate) ;
    }
    }
    if ($message) {
    print "In order to run $0 you need:\n", $message;
    exit;
    }

}

############################################
# Purpose    : Run installation

if ($config->run) {
  ##  check_alpage_dep() if ($config->svn);
  emit_setenv_script();
  update();

  verbose("\n----------------------------------------------------------------------");
  verbose("\n**** Please type 'source $setenv_script' before using the packages");
  verbose("\n**** or/and add it to your favorite shell init file (.bashrc, .bash_profile, ...)");
  if (grep { $_ eq 'parserd' } @packages) {
    if ($config->parserd) {
      system("$parserd_setenv_script restart");
    } else {
      verbose("**** If you wish to run the server of parsers, please type\n\t$parserd_setenv_script start")
    }
  }
  if (grep { $_ eq 'frmg' } @packages) {
    verbose("to play with frmg, you can type frmg_shell");
  }
}
close(LOG);


############################################
# Purpose    : Install or update packages

sub update {

    foreach my $package (@packages) {
    if(!exists $Packg::package_list{$package}) {
        verbose "\nPackage '$package' does not exist...\n";
        die;
    }
    else {
        $Packg::package_list{$package}->update()
        if ($Packg::package_list{$package}->{TYPE});
    }
    }

}

############################################
# Purpose    : Initialize environment variables

sub initenv {

  my @all_aclocals = ();
  foreach my $d (@{$config->aclocaldirs}) {
    (-d $d) and push(@all_aclocals,$d);
  }

    %LENV = (
         PREFIX          => "$prefix",
         BINDIR          => "$prefix/bin",
         SBINDIR         => "$prefix/sbin",
         LIBDIR          => "$prefix/lib",
         DYALOGLIBDIR    => "$prefix/lib/DyALog",
         INCLUDEDIR      => "$prefix/include",
         SHAREDIR        => "$prefix/share",
         MANDIR          => "$prefix/man",
         ACLOCAL         => ["$prefix/share/aclocal",@all_aclocals],
##         ACLOCAL         => ["$prefix/share/aclocal"],
         ETC             => "$prefix/etc",
         VAR             => "$vardir",
         PERL5LIB        => "/usr/share/automake-1.10:$prefix/lib/perl5:$prefix/lib/perl:$prefix/lib/perl5/site_perl:$prefix/lib/perl5/site_perl/$perlversion:$prefix/lib/perl/$perlversion/auto:$prefix/share/automake-1.10:$prefix/share/perl/$perlversion:$prefix/lib/perl/$perlversion:$prefix/lib/perl/5.8:$prefix/lib/share/perl/$perlversion:$prefix/share/perl/5.8:/usr/lib/perl5/vendor_perl/5.8.8",
         CGIDIR          => "$vardir/www/cgi-bin",
         MODPERLDIR      => "$vardir/www/perl",
         INITDIR         => "$prefix/etc/init.d",
         PKG_CONFIG_PATH => "$prefix/lib/pkgconfig",
         PERL_MM_OPT     => "${install_base}=$prefix INSTALLSITEBIN=$prefix/bin INSTALLSITESCRIPT=$prefix/bin INSTALLSITEMAN1DIR=$prefix/man/man1 INSTALLSITEMAN3DIR==$prefix/man/man3",
         PERL_MB_OPT => "--install_base $prefix",
         MODULEBUILDRC   => "$prefix/lib/perl5/.modulebuildrc",
         PERL_CPANM_HOME => "$prefix/src/cpanm",
         PYTHONPATH      => "$prefix/lib"
        );

    #$ENV{LC_ALL}         = "fr_FR";
    $ENV{LD_RUN_PATH}     = "$LENV{LIBDIR}:$LENV{DYALOGLIBDIR}";
    $ENV{LD_LIBRARY_PATH} = "$LENV{LIBDIR}:$LENV{DYALOGLIBDIR}";
    $ENV{LDFLAGS}         = "-L$LENV{LIBDIR}  $ENV{LDFLAGS}";
    $ENV{PERL5LIB}        = "$LENV{PERL5LIB}:$ENV{PERL5LIB}";
    foreach my $path (@{$config->searchdirs}) {
      $ENV{PATH} = "$ENV{PATH}:$path/bin";
    }
    $ENV{PATH}            = "$LENV{BINDIR}:$ENV{PATH}";
    print "PATH is $ENV{PATH}\n";
    $ENV{CPPFLAGS}        = "-I$LENV{INCLUDEDIR}  $ENV{CPPFLAGS}";
    $ENV{PKG_CONFIG_PATH} = $LENV{PKG_CONFIG_PATH};
    ## because option -I is not always passed through autoreconf
  $ENV{ACLOCAL}         = "aclocal ".join(" ",map {"-I $_"} @{$LENV{ACLOCAL}}); 
  ##  print "ACLOCAL $ENV{ACLOCAL}\n";
  $ENV{FTP_PASSIVE}     = 1; # for cpan

  $ENV{PERL_MM_OPT} = $LENV{PERL_MM_OPT}; # for cpan
  $ENV{PERL_MB_OPT} = $LENV{PERL_MB_OPT}; # for cpan
  $ENV{MODULEBUILDRC} = $LENV{MODULEBUILDRC}; # for cpan
  $ENV{PERL_CPANM_HOME} = $LENV{PERL_CPANM_HOME}; # for cpanm

  $ENV{PYTHONPATH} = "$LENV{PYTHONPATH}:$ENV{PYTHONPATH}"; # for python

  #note: $prefix empty at compile time, cannot use it here
  use lib "$LENV{LIBDIR}/perl5/site_perl"; # to be able to check modules versions
  use lib "$LENV{LIBDIR}/perl/$perlversion/auto";
  use lib "$LENV{LIBDIR}/perl5"; # to check modules locally installed with cpan
  use lib "$ENV{PERL5LIB}"; # to check installed modules
}

############################################
# Purpose    : Emit script to initialize environment variables
# Comments   : The user will need to type
#                 source <prefix>/sbin/setenv.sh
#              or add the file to his .bash_profile

sub emit_setenv_script {
  ## Some of the following paths should not be necessary thanks to libtool
  ## but we just add them in case of !
  my $libpath = join(':',
               qw{$ALPAGE_PREFIX/lib/
              $ALPAGE_PREFIX/lib/DyALog/
              $ALPAGE_PREFIX/lib/DyALog/dyalog-xml
              $ALPAGE_PREFIX/lib/DyALog/dyalog-sqlite
            }
              );
  my $version = sprintf("%vd",$^V);

  ## setenv script
  $setenv_script = $LENV{SBINDIR}."/setenv.sh";
  open(SCRIPT,">$setenv_script") || die "can't open $setenv_script: $!";
  my $allpathes = join(":",map {"$_/bin"} @{$config->searchdirs});
  my $alllibs = join(":",map {"$_/lib"} @{$config->searchdirs});
  print SCRIPT <<EOF;
## Source this file or add it to your .bash_profile

## Edit following variable
export ALPAGE_PREFIX="$LENV{PREFIX}"

## Following variables should be ok
#export LC_ALL=fr_FR
export LD_RUN_PATH="$libpath:\$LD_RUN_PATH:$alllibs"
export LD_LIBRARY_PATH="$libpath:\$LD_LIBRARY_PATH:$alllibs"
export LIBRARY_PATH="$libpath:\$LIBRARY_PATH:$alllibs"
export PATH="\$ALPAGE_PREFIX/bin/:\$ALPAGE_PREFIX/share/frmg/:\$ALPAGE_PREFIX/share/biomg/:\$ALPAGE_PREFIX/sbin/:\$PATH:$allpathes"
export LDFLAGS="-L\${ALPAGE_PREFIX}/lib \$LDFLAGS"
export PERL5LIB="\${ALPAGE_PREFIX}/lib/perl5/site_perl:\${ALPAGE_PREFIX}/lib/perl5:\${ALPAGE_PREFIX}/lib/perl5/site_perl/$version:\${ALPAGE_PREFIX}/lib/perl/$version/auto:\${ALPAGE_PREFIX}/share/automake-1.10:\${ALPAGE_PREFIX}/share/perl/$version:\${ALPAGE_PREFIX}/lib/perl/$version:\${ALPAGE_PREFIX}/lib/perl:\${ALPAGE_PREFIX}/lib/perl/5.8:\${ALPAGE_PREFIX}/share/perl/5.8:\$PERL5LIB:/usr/lib/perl5/vendor_perl/5.8.8"
export PKG_CONFIG_PATH="\${ALPAGE_PREFIX}/lib/pkgconfig:\$PKG_CONFIG_PATH"

## for perl and cpan
export PERL_MM_OPT="${install_base}=\${ALPAGE_PREFIX} INSTALLSITEBIN=\${ALPAGE_PREFIX}/bin INSTALLSITESCRIPT=\${ALPAGE_PREFIX}/bin INSTALLSITEMAN1DIR=\${ALPAGE_PREFIX}/man/man1 INSTALLSITEMAN3DIR==\${ALPAGE_PREFIX}/man/man3"
export MODULEBUILDRC="\${ALPAGE_PREFIX}/lib/perl5/.modulebuildrc"

## for python and easy_install
export PYTHONPATH="\${ALPAGE_PREFIX}/lib:\$PYTHONPATH"

## for MacOS
export DYLD_LIBRARY_PATH="\$DYLD_LIBRARY_PATH:$libpath"

EOF
  close(SCRIPT);

  chmod 0755, $setenv_script;
  #verbose("\n**** Please type 'source $setenv_script' before using packages");

  ## parserd_setenv script
  $parserd_setenv_script = $LENV{SBINDIR}."/parserd_setenv";
  open(SCRIPT,">$parserd_setenv_script") || die "can't open $parserd_setenv_script: $!";
  print SCRIPT <<EOF;
source $setenv_script
exec \$ALPAGE_PREFIX/sbin/parserd_service \$*
EOF
  close(SCRIPT);

  chmod 0755, $parserd_setenv_script;
  #verbose("**** Please run $parserd_setenv_script to start/stop parserd server");
}


############################################
############################################
package Packg;
use strict;
use IPC::Run qw( run timeout ) ;
use File::Basename;


our %package_list;

# TODO changer verbose
sub verbose {
    main::verbose (@_);
}

############################################
# Purpose    : create new object package

sub new
{
    my ($class, $fields) = @_;
    my $self = bless $fields, $class;

    # store revision number if present in rev.conf
    if (!$self->{NONLOCALDEP}){
    my $name = $self->{NAME};
    }

    return $self;
}

############################################
# Purpose    : create and register package in %package_list
# Parameters : %package

sub register
{
    my($fields) = @_;
    my %fields = %$fields;
    my $class = 'Packg';
    
    $class .= '::'.$fields{TYPE} if ($fields{TYPE});

    # create and register
    $package_list{$fields{NAME}} = $class->new($fields);
}

############################################
# Purpose    : execute command, print output to log file
# Parameters : $self, %info which contains msg (message to be printed 
#              at execution), cmd and options

sub cmd 
{
    my ($self, %info) = @_;

    verbose "\t$info{msg}" if (defined  $info{msg});
    my $cmd = $info{cmd};
    my $options = $info{options} || [];
    my @linux32 = ();
    push(@linux32,'linux32') if ($config->linux32 && $cmd ne 'xslt-config');

    my $xcmd = `which $cmd`;
    verbose "Running $cmd as $xcmd\n";

    my $out = '';

    # TODO passer LOG en arg
    my $ok = run [@linux32,$cmd,@$options], '>', \$out,'2>',\*main::LOG;
    print( main::LOG $out);

    if (!$ok) {
    return 0 if ($cmd =~ /(pkg|dyalog)-config$/o); # don't exit on this error
    my $error = "$srcdir/error_".$self->{NAME};
    open FILE, ">>$error" or die "$!, stopped"; #keep trace of error for later install
    close FILE;
    my $log = $config->log;
    die "$self->{NAME} not installed, check $log for details, stopped";
    }

    return $out;
}


sub soft_cmd 
{
    my ($self, %info) = @_;

    verbose "\t$info{msg}" if (defined  $info{msg});
    my $cmd = $info{cmd};
    my $options = $info{options} || [];
    my @linux32 = ();
    push(@linux32,'linux32') if ($config->linux32 && $cmd ne 'xslt-config');

    my $out = '';

    # TODO passer LOG en arg
    my $ok = run [@linux32,$cmd,@$options], '>', \$out,'2>',\*main::LOG;
    print( main::LOG $out);
    return ($ok,$out);
}


############################################
# Purpose    : fetch tar.gz from ftp
# Returns    : 1 if new release was retrieved

sub ftp {
  my $self = shift;
  return $self->{FTP};
}

sub fetch_from_ftp {
    my $self = shift;

    my $url=$self->ftp;
    unless ($url) {
      die "$!\ncannot fetch from FTP: no FTP address provided in main file, stopped";
    }
    my $local = basename($url);
    my $localdir = basename($local,'.tar.gz');

    # fetch tarball unless it is already there
    if (-e $local && !$config->force) {
    chdir $self->{NAME};
    return 0;
    }

    # remove directory (otherwise there is a pb with rename, and if previous 
    # version was fetched from svn, .svn/ needs to be deleted anyway)
    if (-d "$self->{NAME}") {
      my $backup = "bak.$$";
      rename($self->{NAME},"$self->{NAME}.$backup") || die "Can't rename previous version of $self->{NAME}: $!";
    }
    

    $self->cmd( msg => "fetching online",
        cmd => "wget",
        # last option to be able to download from gforge
        options => [$url, "-nd", "-N", "-r", "--no-check-certificate"] 
        );   

    $self->cmd( msg => "untar",
        cmd => "tar",
        options => ["xzvf","$local"]
        );
    
    if (!-e $localdir) {
      verbose "Default localdir=$localdir not found for package '$self->{NAME}'";
      ## a problem with the tar or the package was un-tarred under
      ## some strange name (eg. sqlite)
      my @candidates = glob("$self->{NAME}*");
      foreach my $x (@candidates) {
    if (-d $x) {
      $localdir = $x;
      verbose "Using localdir=$x for package '$self->{NAME}'";
      last;
    }
      }
    }

    # just keep the package name (without the version number)
    if (-e $localdir && $localdir ne $self->{NAME}) {
    rename($localdir, $self->{NAME}) or die "can't rename: $!, stopped";
    }
    chdir $self->{NAME};
    return 1;
}

############################################
# Purpose    : test existence of ftp file
# Returns    : 1 if ok, 0 unless
sub check_ftp_path {
  my $self = shift;
  my $url = $self->{FTP};
  my $name = $self->{NAME};
  my ($ok,$msg) = $self->soft_cmd( msg => "fetching online",
                   cmd => "wget",
                   # last option to be able to download from gforge
                   options => [$url, "--spider","--no-check-certificate"] 
                 );
  if ($ok) {
    ##    print "Package '$name': ftp path ok\n";
  } else {
    print "Package '$name': ftp path NOT ok '$url'\n";
  }
  
}

############################################
# Purpose    : checkout or update sources from Subversion repository
# Returns    : 1 if new revision was retrieved

sub fetch_from_svn {
    my $self = shift;
    my $dev = $config->svn;

    # revision: number specified by the developper or last revision (HEAD)
    my $rev = '-r';
    if (exists $self->{REVISION}) {
    $rev .= "$self->{REVISION}";
    }
    elsif ($config->date) {
    my $date = $config->date;
    $rev .= "{$date}";
    }
    else {
    $rev .= 'HEAD';
    }

    # if there is no working copy, then checkout
    if (!-d "$self->{NAME}/.svn/") {

    # remove directory if previous version was fetched from ftp
    # (if pkg directory exists and there is no .svn/ directory in it,
        # then it means the previous version was fetched from ftp)
    die "Please remove previous version of $self->{NAME} (not a working copy)."
        if (-d $self->{NAME});

    if ($dev ne 'anonymous') {

        if ( $self->cmd( msg => "fetching from svn+ssh",
                 cmd => 'svn',
                 options => [qw/co /, $rev, "svn+ssh://".$dev."@".$self->{SVN}, $self->{NAME}] ) 
         eq '0' ) {
        
          print STDERR "cannot fetch from svn with ssh: check gforge name or ssh key\n";
          print STDERR "trying alternate access (for instance anonymous access)\n";
          
          if ( $self->cmd( msg => "fetching from svn with https",
                   cmd => 'svn',
                   options => [qw/co /, $rev, '--username',$dev,"https://".$self->{SVN}, $self->{NAME}] ) 
           eq '0' ) {
        print STDERR "cannot fetch from svn with https: check gforge\n";
          }
        }
    }
    elsif ($config->update) {
      ## anonymous access
      my $mode = $config->anon_mode_https;
      if ($mode) {
        # use https://...
        $self->cmd( msg => "fetching from svn",
            cmd => 'svn',
##            options => [qw/co /, $rev, "svn://".$self->{SVN}, $self->{NAME}] 
            options => [qw/co /, $rev,'--username','anonsvn','--password','anonsvn',"https://".$self->{SVN}, $self->{NAME}] 
            );
      } else {
        # use svn://..
        $self->cmd( msg => "fetching from svn",
            cmd => 'svn',
            options => [qw/co /, $rev, "svn://".$self->{SVN}, $self->{NAME}] 
              );
      }
    } else {
      ## no svn update with option -noupdate
      ##      print "Package '$self->{NAME}': no svn update\n";
    }

    chdir $self->{NAME};
    my $v = $self->cmd( cmd => 'svnversion',
                options => ["."]
                );
    chomp $v;
    $self->{V} = $v;
    }

    elsif (-d "$self->{NAME}/.svn/"){ 
    chdir $self->{NAME};    
    my @extra = ();
    if ($config->anon_mode_https || $config->svn eq 'anonymous') {
      push(@extra,'--username','anonsvn','--password','anonsvn');
    }
    my $out = $self->cmd( msg => "updating from svn",
                  cmd => 'svn',
                  options => [qw/up ./, @extra,$rev]
                  );

    my $n = () = ($out =~ m/\n/g);

    my $v = $self->cmd( cmd => 'svnversion',
                options => ["."]
                );
    chomp $v;
    $self->{V} = $v;

    return 0 if ($n == 1 && !$config->force);
    }
    return 1;
}

############################################
sub conda {
  my $self = shift;
  return $self->{CONDA};
}

sub conda_channel {
  my $self = shift;
  return $self->{CONDA_CHANNEL};
}

sub fetch_from_conda {
  my $self = shift;
  my $name=$self->conda;
  my $channel=$self->conda_channel;

  # add test to verify if package already there

  $self->cmd( msg => "installing with conda",
      cmd => "conda",
      options => ["install", $name, "--channel", $channel] 
      );     
  chdir $self->{NAME};

  print `pwd`;
  
  return 1;
}

############################################
# Purpose    : fetch git
# Returns    : 1 if new release was retrieved

sub git {
  my $self = shift;
  return $self->{GIT};
}

sub fetch_from_git {
  my $self = shift;
  my $url=$self->git;

  # if there is no git working copy, we clone the project
  if (!-d "$self->{NAME}/.git/") {

    # remove directory if previous version was fetched from ftp
    # (if pkg directory exists and there is no .git/ directory in it,
    # then it means the previous version was fetched from ftp or svn)
    die "Please remove previous version of $self->{NAME} (not a working copy)."
        if (-d $self->{NAME});

    # remove directory (otherwise there is a pb with rename, and if previous 
    # version was fetched from svn, .svn/ needs to be deleted anyway)
    if (-d "$self->{NAME}") {
      my $backup = "bak.$$";
      rename($self->{NAME},"$self->{NAME}.$backup") || die "Can't rename previous version of $self->{NAME}: $!";
    }

    $self->cmd( msg => "installing with git",
        cmd => "git",
        options => ["clone", $url, $self->{NAME}] 
        #options => ["clone", "--single-branch", "--no-tags", $url] 
        );     
    chdir $self->{NAME};

  # If there is a git working copy
  } else {
    # We update the existing projet
    if ($config->update) {
      chdir $self->{NAME};
      $self->cmd( msg => "updating with git",
        cmd => "git",
        options => ["pull"] 
        );         
    # We reclone the project
    } else {
      if (-d "$self->{NAME}") {
        my $backup = "bak.$$";
        rename($self->{NAME},"$self->{NAME}.$backup") || die "Can't rename previous version of $self->{NAME}: $!";
      }
      $self->cmd( msg => "reinstalling with git",
          cmd => "git",
          options => ["clone", $url, $self->{NAME}] 
         #options => ["clone", "--single-branch", "--no-tags", $url] 
          );    
      chdir $self->{NAME}; 
    }
  }

  print `pwd`;
  
  return 1;
}

############################################
# Purpose    : test existence of ftp file
# Returns    : 1 if ok, 0 unless
sub check_git_path {
  my $self = shift;
  my $url = $self->{GIT};
  my $name = $self->{NAME};
  my ($ok,$msg) = $self->soft_cmd( msg => "fetching online",
                   cmd => "wget",
                   # last option to be able to download from gforge
                   options => [$url, "--spider","--no-check-certificate"] 
                 );
  if ($ok) {
    ##    print "Package '$name': ftp path ok\n";
  } else {
    print "Package '$name': git path NOT ok '$url'\n";
  }
  
}

############################################
# Purpose    : Download package sources
# Returns    : 1 if new version was retrieved

sub download
{
    my $self = shift;

    chdir $srcdir;

    if ($self->(CONDA)){
    	return $self->fetch_from_conda
    }

    if ($self->{GIT}) {
    	return $self->fetch_from_git;
    }

    if ($config->svn && $self->{SVN} 
    && !(exists $self->{PRIVATE} && $config->svn eq 'anonymous')
    ){
        return $self->fetch_from_svn;
    }
    else {
	    if ($self->{FTP}) {
	        return $self->fetch_from_ftp;
        }
        print "There is no release for this package.";
        return 0;    
    }
}

############################################
# Purpose    : Update parserd.conf
# Comment    : update parserd.conf without overwriting info

sub update_conf 
{
    ## done in parserd
    my $self = shift;

    my $conffile = "$LENV{ETC}/parserd.conf";
    verbose "update_conf $conffile";
    open(CONF,"<$conffile") or die "can't postprocess $conffile: $!, stopped";

    my $info;
    my $parsers;
    while(<CONF>) {
    #$content .= "$_" unless (/^\#\# Local options/ || /^port/ || /^user/ || 
        #         /^group/ || /^log_file/ || /pid_file/ || /^path/);

    unless (/^\#\# Local options/ || /^port/ || /^user/ || 
        /^group/ || /^log_file/ || /pid_file/ || /^path/) {
        if (/^\# /) {
        $info .= "$_";
        }
        else{
        $parsers .= "$_";
        }
    }
    }

    close(CONF);

    open(CONF,">$conffile") or die "can't postprocess $conffile: $!, stopped";
    my $port    = $config->port;
    my $user    = $config->user;
    my $group   = $config->group;

    print CONF <<EOF;
$info

## Local options
port = $port
user = $user
group = $group
log_file = $LENV{VAR}/log/parserd.log
pid_file = $LENV{VAR}/run/parserd.pid
path  = $LENV{BINDIR}
path  = /usr/local/bin
path  = /usr/ucb

$parsers
EOF
}

############################################
# Purpose    : Update dependencies of this pkg before updating it

sub update_depend 
{
  my $self = shift;

  my @depend = ();

  if ($config->svn) {
    @depend = keys %{$self->{SVNDEPEND}};
  }

  foreach my $dep ( sort (@depend,keys %{$self->{DEPEND}}) ) {
    die "Package '$dep' is not listed in this script" unless ($package_list{$dep});
    $required{$dep} = 1;    # depenencies should be handled even if in the skip list
    $package_list{$dep}->update();
  }
}

############################################
# Purpose    : Get version number
# Returns    : Version number

sub version
{
    my $self = shift;

    my $cmd = @{$self->{VERSION}}[1];
    my @options = split (/ /, @{$self->{VERSION}}[2]);

    my @searchdirs = (split(/:/,$ENV{PATH}),
              (map {"$_/bin"} @{$config->searchdirs}),
              $LENV{BINDIR},
              "$ENV{HOME}/bin");

    my $fullcmd='';
    foreach my $path (@searchdirs) {
      if (-x "$path/$cmd") {
    $fullcmd = "$path/$cmd";
    last;
      }
    }

    if (1) {
      if ($fullcmd) {
    print "$cmd => $fullcmd\n";
      } else {
    print "$cmd => not found in @searchdirs\n";
      }
    }

#    return 0 if ((!-x "/usr/bin/$cmd") && (!-x $LENV{BINDIR}."/$cmd") && (!-x $ENV{HOME}."/bin/$cmd") ) ;
    return 0 unless ($fullcmd);
    my $version = $self->cmd( cmd     => $fullcmd,
                  options => [ @options ]);
    chomp $version;
    ($version) = ( $version =~ m/(\d[.\d]*)/ ) if ($version =~ m/^\D/); # extract version number
    return $version if($version);
    return 0;
}

############################################
# Purpose    : Check if package is up to date
# Returns    : 1 if up to date, else 0

sub uptodate
{
    my $self = shift;
    my $name = $self->{NAME};

    return 0 if (($config->force && !$self->{ALPAGEDEP}) ||
    (!exists $self->{VERSION} && !exists $self->{INSTALLED}));

    # check version
    my $version;

    if (exists $self->{VERSION}) {

    $version = $self->version();
    return 0 if (!$version);
    my @reqversion  = split /\./, ${$self->{VERSION}}[0];
    my @instversion = split /\./, $version;
    
    foreach my $num (@reqversion) {
      my $inst = shift @instversion;
      return 0 if ($num > $inst);
      last if ($inst > $num);
    }
      }
    
    if (exists $self->{PKGCHECK}) {
      my ($reqpkg,$reqversion) = @{$self->{PKGCHECK}};
      verbose "checking installion $name: pkgcheck $reqpkg $reqversion";
      system("pkg-config --exists --print-errors \"$reqpkg >= $reqversion\"") and return 0;
      verbose "checking installion $name: pkgcheck ok";
    }

    # check if a file is missing
    if (exists $self->{INSTALLED}) {
      my $installed = 1;
      foreach my $file (@{$self->{INSTALLED}}) {
    my @where=();
      if (!(@where = grep {-e "$_/$file"} @{$config->searchdirs})) {
        if ($file =~ /\.la$/) {
          my $altfile = $file;
          my $suff = ($^O =~ /darwin/) ? 'dylib' : 'so';
          $altfile =~ s/\.la$/.$suff/o;
          if (!(@where = grep {-e "$_/$altfile"} @{$config->searchdirs})) {
        $installed = 0;
        last;
          }
        } else {
          $installed = 0;
          last;
        }
      }
      verbose("checking instalation $name: found file $where[0]/$file");
    }
    unless ($installed) {
        foreach my $file (@{$self->{INSTALLED}}) {
        return 0 if (!-e "$LENV{PREFIX}/$file");
        }
    }
    }

    my $uptodate = "$self->{NAME} is up to date";
    $uptodate   .= " ($version)" if ($version);
    verbose $uptodate unless $config->dependencies;
    $self->{UPDATED} = 1;

    return 1;
}


############################################
# Purpose    : Postprocess after make install

sub postprocess 
{
    my $self = shift;

    while ( my ($process, $args) = each( %{$self->{POSTPROCESS}} ) ) {

    if    ($process eq 'bininstall') {
        foreach my $binary (@$args) {
        $self->cmd( msg => "install $binary",
                cmd => 'install',
                options => ["$binary","$LENV{BINDIR}/$binary"]
                );
        }
    }
    elsif ($process eq 'pkgshareinstall') {
        my $pkgsharedir = "$LENV{SHAREDIR}/$self->{NAME}";
        mkdir $pkgsharedir;

        foreach my $file (@$args) {
        $self->cmd( msg => "install $file",
                cmd => 'install',
                options => ["$file","$pkgsharedir/$file"]
                );
        }
    }
    elsif ($process eq 'cmd') {
        $self->cmd(%$args);
    }
    elsif ($process eq 'update_conf') {
        $self->update_conf();
    }
    }
}

############################################
# Purpose    : Install or update package

sub update 
{
    my $self = shift;

    ##    Packg::verbose "Update $self->{NAME}";
    
    foreach my $pkg (@{$config->skippkg}) {
      # pkg explicitely required by the user are installed (even if in the skip list)
      return if ($pkg && $self->{NAME} eq $pkg && !exists $required{$pkg});
    }
    
    return if ($self->{UPDATED} || (($config->ftp || !$self->{SVN}) && $self->uptodate));
    #return if ($self->{GIT} && $self->uptodate);

    $self->update_depend() unless ($config->skipdep); # update dependencies first

    Packg::verbose "----------------\nHandling package $self->{NAME}\n----------------";
    # eventual error flag from previous install
    my $error = "$srcdir/error_".$self->{NAME};

    # different according to pkg type (Autotools, Perl...)
    if ($self->can("cpan") && $self->{NONLOCALDEP}) {
      $self->cpan;
    } else {
      $self->download();
      $self->create_make(); 

      if (-f "Makefile" && ! exists $self->{NOMAKE}) {
          $self->cmd( msg => "make", cmd => 'make');
        ## test alpage packages
        $self->test() if ($config->test && !$self->{NONLOCALDEP} && ($self->{NAME} ne "syntax"));
        exists $self->{NOINSTALL} or $self->cmd( msg => "install", cmd => 'make', options => ['install']);
      }
      $self->postprocess if ($self->{POSTPROCESS});
    }

    chdir "$config->prefix/";
    $self->{UPDATED} = 1;
    unlink $error;
    verbose "done";
}

############################################
# Purpose    : Create distribution

sub makedist
{
    my $self = shift;
    chdir "$srcdir/".$self->{NAME};
    $self->cmd( msg => "make dist", cmd => 'make', options => ['distcheck']);
    verbose "done";
}

1;


#################################
#################################
package Packg::Autotools;

use strict;
use File::Basename;
use base qw/Packg/;

# this pkg uses autotools

sub autoreconf
{
    my $self = shift;

    exists $self->{NOAUTORECONF} and return;
    
    if(($self->{NAME} eq 'DyALog') && ($Packg::package_list{autoconf}->version < 2.60)){    
        # DyALog has incompatibility with autoconf < 2.60
        # _AC_SRCDIRS replaced by _AC_SRCPATHS in configure.ac
        my $dir = fileparse("$srcdir/DyAlog/");
        my $old = $dir."configure.ac";
        my $new = $dir."configure.ac.new";
        open(OLD, "<", $old) or die "can't open $old: $!";
        open(NEW, ">", $new) or die "can't open $new: $!";
        while (<OLD>) {
            if (/^_AC_SRCDIRS/) {
                print NEW "_AC_SRCPATHS([\".\"])";
            } else {
                print NEW $_;
            }
        }
        close(OLD)                or die "can't close $old: $!";
        close(NEW)                or die "can't close $new: $!";
        rename($old, "$old.orig") or die "can't rename $old to $old.orig: $!";
        rename($new, $old)        or die "can't rename $new to $old: $!";
    } 
    ## For all other projects:
    # export automake env variable
    $ENV{AUTOMAKE} = ((exists $self->{DEPEND}->{DyALog}) ? "automake-dyalog" : "automake" );

    if(exists $self->{NEEDLIBTOOL}) {
    	if ($self->is_libtool() eq "glibtool") {
   			$self->cmd(msg     => 'glibtoolize', 
            		   cmd     => 'glibtoolize');
   		} else {
      	$self->cmd(msg     => 'libtoolize', 
          		    cmd     => 'libtoolize');
   		}
        #$self->cmd(msg     => 'aclocal', 
        #   cmd     => 'aclocal');
        #$self->cmd(msg     => 'autoconf', 
        #   cmd     => 'autoconf');
        #$self->cmd(msg     => "$ENV{AUTOMAKE} --add-missing ", 
        #   cmd     => "$ENV{AUTOMAKE}", 
        #   options => ["--add-missing"]);
        #return;
    } #else {
    $self->cmd(msg     => 'autoreconf', 
       		   cmd     => 'autoreconf', 
       		   options => [ "-f", "-i", "-v"]); #,
    #}

    delete $ENV{AUTOMAKE};
}

sub is_libtool {
  if ($^O =~ m/darwin/) {
    return "glibtool";
  } 
  return "libtool";
}

sub configure
{
  my $self = shift;

  exists $self->{NOCONFIGURE} and return;
  
  my $prefix = $config->prefix;
  my @options = ("--prefix=$prefix");
  push(@options,@{$self->{CONFIGURE}}) if (exists $self->{CONFIGURE});
  push(@options,'--enable-rawscripts') if ($self->{NAME} eq 'lefff' && $config->linux32);
  $self->cmd( msg => "configure @options", 
          cmd => "./configure",
          options => [ @options ]);
}

############################################
# Purpose    : Generate Makefile

sub create_make {
    my $self = shift;
    $self->autoreconf() if (exists $self->{AUTORECONF});
    
    $self->configure();

    # change .ma files date when svn checkout (DyALog being bootstrapped)
    if (($self->{NAME} eq 'DyALog'
     || $self->{NAME} eq 'frmg')
    && $config->svn) {
    $self->cmd( msg     => "make postsvn", 
            cmd     => "make",
            options => [ "postsvn" ]);
    }
}

sub test {
    my $self = shift;
    $self->cmd( msg     => "make check", 
        cmd     => "make",
        options => [ "check" ]);
}

1;



#################################
#################################
package Packg::Perl;

use strict;
use base qw/Packg/;

# this pkg is a perl pkg

############################################
# Purpose    : Get version number

sub version {
    my $self = shift;
    my $mod = @{$self->{VERSION}}[1];

    my $v = `perl -M$mod -e 'print \$$mod\:\:VERSION' 2> /dev/null`;

    return 0 if (!$v);
    return $v;
}

############################################
# Purpose    : Generate Makefile

sub create_make {
    my $self = shift;

    # create Makefile
    $self->cmd( msg => "configure (with Perl)",
        cmd => "perl",
        options => ['Makefile.PL',
                "${install_base}=$LENV{PREFIX}",
                "INSTALLSITEBIN=$LENV{BINDIR}",
                "INSTALLSITESCRIPT=$LENV{BINDIR}",
                "INSTALLSITEMAN1DIR=$LENV{MANDIR}/man1",
                "INSTALLSITEMAN3DIR=$LENV{MANDIR}/man3",
                @{$self->{CONFIGURE} || []}
                ]
        ); 
}

sub test {
    my $self = shift;
    $self->cmd( msg     => "make manifest", 
        cmd     => "make",
        options => [ "manifest" ]);
    $self->cmd( msg     => "make test", 
        cmd     => "make",
        options => [ "test" ]);
}

sub cpan {
  my $self = shift;
  my $name = $self->{NAME};
  my $modname = $name;
  my $version = undef;
  my $fullname = undef;
  if (exists $self->{VERSION}) {
    $version = $self->{VERSION}[0];
    $modname = $self->{VERSION}[1];
    $fullname = "$modname~$version";
  } else {
    $modname =~ s/-/::/og;
    $fullname = $modname;
  }
  $self->cmd( msg => "installing with cpanm",
          cmd => "$cpanm",
          options => ['--skip-satisfied',$fullname]
        );
  #  system("$cpanm --skip-satisfied $fullname")
  #    or die "couldn't install CPAN module $modname $version"
  #  ;
}

1;

#################################
#################################


=head1 NAME

alpi -- Alpage Installer

=head1 DESCRIPTION

B<alpi> is a Perl script designed to ease a local installation of
software developed by INRIA team Almanach (a follow-up of ALPAGE).
While not perfect, it is quite useful because it detects dependencies
and try to install them automatically.

It can also be used to install a full linguistic processing chain for
French based on B<DyALog> (a parser compiler and logic programming
enviromnent), B<FRMG> (a French Meta Grammar), B<Lefff> (a French
Morphological and Syntactic Lexicon) and B<SxPipe> (a pre-parsing
chain). B<Parserd>, a server of parsers, is also installed allowing to
process whole corpus with the perl scripts B<callparser> or
B<dispatch.pl>.

More information may be found at

=over 4

=item * Alpage software: L<http://alpage.inria.fr/software.en.html>

=item * FRMG Wiki: L<http://alpage.inria.fr/frmgwiki>, where FRMG may be tried online

=item * Installing FRMG: L<http://alpage.inria.fr/frmgwiki/wiki/installer-la-chaine-alpage>

=item * Demo of the linguistic processing chain: L<http://alpage.inria.fr/parserdemo>

=back

Please be sure to consult the TROUBLESHOOTING section in case of problems

=head1 USAGE

To install the full Alpage chain (under F<~/exportbuild>)

   ./alpi

To install a specific Alpage package, you just need to type in a
terminal:

   ./alpi --pkg=packagename

More than one package with:

   ./alpi --pkg=pkg1 --pkg=pkg2

To install the whole Alpage linguistic processing chain, just type
'./alpi'. See below for more details and examples.

Please note that you do not need to be root to run B<alpi> because the
software is installed locally (by default under F<~/exportbuild>). In
future development it will be also possible to install as root.

There are three possibilities to download Alpage software
with B<alpi>:

1. [Default behaviour] B<alpi> retrieves the stable version
downloading the tar.gz releases through FTP

Example : install B<DyALog> with B<alpi> from FTP:

    ./alpi --pkg=DyALog

2. OR, preferably, you can retrieve the version based on the latest sources
(from our Subversion repositories) 

2.1. You don't have an INRIA GForge account. You can retrieve the sources 
with anonymous checkout.

    ./alpi --svn=anonymous --pkg=DyALog

B<Note>: when retrieving the sources anonymously, you will not be able to
contribute to the code.

2.2. You have an INRIA GForge account and are registered to our GForge
projects. (See information about option svn below for more details.)

    ./alpi --svn=mylogin --pkg=DyALog

When the install is finished, B<alpi> sets up your environment variables
so that you can use Alpage software directly. You can set up that
environment yourself typing :

    source ${PREFIX}/sbin/setenv.sh

This environment is set for that session only. In order to keep it
permanently, you may add the content of F<setenv.sh> in the file
F<.bashrc> (in your homedir).

It is strongly recommended to look at file alpi.log (which is in same
directory as B<alpi> script) to search for errors, because B<alpi>
does not explicitly mention them !

In case of trouble and when contacting the maintainers of the B<alpi>
script, please attach a copy of alpi.log.

=head1 OPTIONS

=over 4      

=item --help|--usage                 this help 

=item --config=F<file>               to read a specific config file [default F<alpi.conf>]

=item --prefix=F<path>               default prefix path [default F</home/toto/exportbuild>]

=item --src=F<path>                  default path for sources [default F</home/toto/exportbuild/src>]
       
=item --vardir=F<path>               default path for dynamic and tmp data [default F</tmp/toto/exportbuild/var>]      
       
=item --port|p=<number>              default port for the optional parser server [default 8999] 
       
=item --user|u=<user>                default user for the optional parser server

=item --group|g=<group>              default user for the optional parser server
       
=item --svn=<gforge_login>           to download sources from the svn repository on Gforge

=item --date=<date>                  date to fetch old versions in the subversion repository
       
=item --package|pkg=<package>        to install a specific package

=item --skippkg=<package>            to skip installation of specified package

=item --skipdep=<package>            to skip installation of all dependencies
       
=item --linux32                      B<deprecated> -- to force 32bit emulation mode on 64bit machines
       
=item --force|f                      to force the installation of a package, even if already installed
       
=item --dependencies|dep             to list the dependencies needed to run this script
       
=item --version|v                    to print versions of Alpage packages

=item --project                      to print Gforge project name of each package

=item --info                         to display information about the packages installed by B<alpi>

=item --log=<log file>               the log file for B<alpi> [default alpi.log] 

=item --test                         to run packages tests with no real installing [default no]

=item --biotim                       to install the biotim chain [default no]
       
=back

=head2 --config

You can write a small configuration script called alpi.conf and
placed in the same directory as alpi. This script could
look like this:

  svn= mylogin
  pkg= DyALog
  pkg= lefff
  force
  prefix= /home/toto/somedir

Then you only need to type B<./alpi> to run the script. If
you want to add an option, place it in this configuration file.

=head2 --prefix
    
Everything is installed under the path specified with option
--prefix [the default is ${HOME}/exportbuild].

=head2 --src
    
Where you can find the packages sources. The default location is 
${HOME}/exportbuild/src.

=head2 --user and --group

These options are useful if you install parserd which provides
a server of parsers. This server will run under user and group. When the
values are not provided by the user, the script retrieves user
name under which it runs, and the first group the user belongs to.

=head2 --svn

B<alpi> can either download the latest stable
releases from FTP, or retrieve the latest sources from the Subversion
repository (svn) on Inria Gforge.

The stable version is retrieved by default.

To retrieve the latest sources from the Subversion repository, there
are two possibilities. 

1. Get the sources anonymously:

    ./alpi --svn=anonymous

2. If you are a developper among our projects on INRIA GForge, please provide
your gforge login:

    ./alpi --svn=mylogin

If you do not have a GForge account yet, you can create one filling
the following form:
L<https://gforge.inria.fr/account/register.php>.
Then you should contact the author of this script to get access to
the sources (some are public, other private). You will be
registered in our GForge projects Alpage
Linguistic Workbench, DyALog, Metagrammar Toolkit, Syntax and Alexina.

Then you need to set up ssh (this section comes from the GForge FAQ
L<http://siteadmin.gforge.inria.fr/FAQ.html#Q6>):

Generate a pair of rsa public/private keys:

    ssh-keygen -t rsa

Paste your public key (/.ssh/id_rsa.pub) in the gforge website. To
do this, you need to go to your account and then go to the Account
maintainance tab. At the bottom of the Account maintainance tab,
you should see a Shell Account Information section which contains
an [Edit keys] link. Paste your public key(s) in the empty field
below and click the Update  button.

! A common problem is that the Shell Account Information field does
not appear in your account page. This usually happens because
you do not belong to any gforge project.

! Please, be aware that uploading your ssh public key on the
server will not allow you to connect to the server immediately
through ssh. To do so, you will need to wait at most 24h. If your
connection is impossible 24h later, please, contact the server
administrators.

Finally you should set up a ssh agent, so that you will not be
prompted to type your passphrase each time B<alpi> accesses the
Subversion repository:

    eval `ssh-agent`
    cp /etc/ssh/ssh_config .ssh/config # ForwardAgent variable is set to``yes''
    ssh-add

The ssh-agent is started in the beginning of an X-session or a login
session.

=head2 --date

Retrieve revisions of the sources at a specific date. This is the syntax:

=over 4

=item 2006-02-17 (Subversion assumes a time of 00:00)

=item 20060217T1530

=back

    ./alpi --svn=toto --date=2006-02-17

If you don't specify option svn, there will be an anonymous checkout.
Option date is global (not package specific).

=head2 --package|pkg

To install a specific package: --package=somepkg (or --pkg=somepkg)
Examples:

    ./alpi --package=DyALog
    ./alpi --pkg=DyALog --pkg=frmg

To install a special revision of the Subversion repository for a 
package, type:

    ./alpi --pkg=<package name>-r<revision number>

For example, get revision 1144 of package sxpipe:

    ./alpi --pkg=sxpipe-r1144 --svn=mylogin
    ./alpi --pkg=sxpipe-r1144

Note that if you have a GForge account and forget to provide it, the 
checkout will be anonymous.

=head2 --skippkg

You can specify which packages you don't want to be installed.

    ./alpi --skippkg=mgtools


=head2 --linux32 (deprecated)

This option may be used to force the 32bit emulation mode on 64b
machines. The ALPAGE tools are now 64bit-compatible and this option is
therefore no longer necessary.

=head2 --force|f

force causes the script to start from 'autoreconf' instead of
'make' when the sources are retrieved from the Subversion
repository. When the sources are retrieved by FTP, the tarball
is downloaded and decompressed again.

=head2 --test

With this option B<alpi> will run tests before installing
any package ('make check' for packages using autotools, 'make
test' for Perl packages). This is recommended if you are a
developper.

=head2 --searchdirs=<prefix path>

This option may be used to add a new prefix path used to search
programs and libraries. This option may be used more than once.

=head2 --noupdate

This option may be used to skip the SVN or FTP update phase for the
packages, when already present.

=head2 --parserd

This option may be used to start the server of parsers B<parserd> at
the end of the installation process. This option adds the B<parserd>
package to the list of packages to install.

=head1 Alpage software installed by alpi

=over 4

=item * DyALog: a parser compiler and logic programming environment

=item * dyalog-xml: A DyALog module to access LibXML

=item * dyalog-sqlite: A DyALog module to access SQLITE3 databases

=item * sxpipe: a pre-parsing chain for French

=item * Lingua-Features: Natural languages features

=item * forest_utils: Perl conversion scripts for Shared Derivation Forests

=item * parserd: A server of parsers with clients

=item * mgcomp: A Meta-Grammar compiler written in DyALog

=item * mgtools: An enviromnent to edit and view Meta-Grammars

=item * frmg: A French Meta Grammar

=item * alexina-tools

=item * lefff: A French Morphological and Syntactic Lexicon

=item * lefff-frmg: Adaptation of Lefff for FRMG

=item * syntax: To compile tools that rely on SYNTAX

=back

=head1 DEPENDENCIES

=over 4

=item * connection to Internet :) (because the script retrieves the packages through FTP or Subversion)

=item * Perl >= 5.8

=item * AppConfig (B<now installed by alpi>)

=item * IPC::Run (B<now installed by alpi>)

=item * pkg-config 0.15.0 (B<now installed by alpi>)

=item * g++

=item * perlcc: for Lefff (perlcc is part of Mandriva rpm perl-devel)

=item * xsltproc: for frmg (B<now installed by alpi>)

=item * yacc (bison > 2.3 or byacc): for DyALog (B<now installed by alpi>)

=item * flex: for DyALog (B<now installed by alpi>)

=item * telnet: for parserd, if you need to use callparser

=back

If you retrieve the sources from the Subversion repository:

=over 4

=item * Subversion !

=item * autoconf >= 2.60   (B<now installed by alpi>)

=item * automake >= 1.10   (B<now installed by alpi>)

=item * libtool >= 1.5     (B<now installed by alpi>)

=item * makeinfo: for DyALog (makeinfo is part of Mandriva rpm texinfo)

=back

Optional dependencies:

=over 4

=item * recode: for parserd (callparser)...

=item * ImageMagick: to display a graph of dependencies (command display or option -dep of callparser (package parserd)

=item * dillo: to display HTML streams produced by callparser (package parserd)

=item * uDraw (formerly known as daVinci): view Meta-Grammars hierarchies with mgviewer (package mgtools)

=back

=head1 PERL MODULES

Installing locally the Perl modules recursively with all their
dependencies may be a difficult task. Thanks to B<cpanm> and ideas
inspired from B<local::lib>
L<http://perl.jonallen.info/writing/articles/install-perl-modules-without-root>,
things are getting much easier.

=head1 DOWNLOAD ALPI

The latest release can be retrieved from the following WEB page: 
L<https://gforge.inria.fr/frs/?group_id=481>

Preferably, the latest version of this script can also be retrieved
from the Subversion repository typing:

    svn co svn://scm.gforge.inria.fr/svn/lingwb/alpi/trunk alpi

or, 

    svn co --username=anonsvn --password=anonsvn https://scm.gforge.inria.fr/svn/lingwb/alpi/trunk alpi

If you have already made a checkout of B<alpi>, in order to
check that you have the latest version, cd in its directory and type

    svn update

=head1 TROUBLESHOOTING

A short FAQ about some common problems. Please feel free to send us
information about your own problems.

=head2 XML::Parser does not compile.

The problem may come from B<expat> not being found. Check that the
expat shared library is installed, also also the header file
F<expat.h> (possibly by installing a I<devel> package with your
distribution). If these files are not installed in a standard place,
you can use either the B<--expat_prefix> option of B<alpi> or the
shell environment variable B<EXPATINCPATH> and B<EXPATLIBPATH>.

=head2 lefff is recompiled each time and it is very slow (svn mode)

After B<lefff> has been installed, you may add the option
B<--skippkg=lefff> to the next invocations of B<alpi> to avoid a
recompilation of lefff (that induces a recompilation of B<sxpipe> and
a few other packages).

=head2 frmg doesn't compile or the compilation doesn't finish

Your computer may not have enough RAM and the compilation process is
swapping. In ftp mode, the archive includes an expanded form of the
grammar and that case should no longer arise. In svn mode, a dump of
the meta-grammar compilation is provided that should limit this
problem but you may still need 1Go of RAM (and more is better !).

=head2 configure for dyalog-sqlite or dyalog-xml fails with some message about PKG_CHECK_MODULES

Please check if some m4 files for sqlite or libxml was not installed
in some aclocal directory not examined by your version of aclocal, for
instance in F</usr/share/aclocal/> or F</usr/local/share/aclocal>.
This case may arise if aclocal was installed by alpi, but sqlite or
libxml was installed by your distribution.

If it is the case, run B<alpi> with option
B<--aclocaldirs=myextrapath>

=head2 I would like to add some optional Perl modules required in some scripts provided by the ALPAGE chain

B<alpi> installs the B<cpanm> script
L<|http://search.cpan.org/dist/App-cpanminus/>, a very useful script
for locally installing Perl modules. If, for instance, you wish to
install the non mandatory module B<Term::ReadLine::Gnu> (useful for
B<frmg_shell>), do type:

    cpanm Term::ReadLine::Gnu

Please look the documentation of B<cpanm> for more details

=head1 AUTHORS

=over 4

=item Eric de la Clergerie, <Eric.De_La_Clergerie@inria.fr>

=item Isabelle Cabrera, <Isabelle.Cabrera@inria.fr>

=back

=head1 SEE ALSO

=over 4

=item B<alpi> home page : L<http://alpage.inria.fr/alpi.en.html>

=item FAQ : L<http://alpage.inria.fr/alpchainfaq.en.html>

=item B<alpi> mailing list: L<https://gforge.inria.fr/mail/?group_id=481>

=item Alpage home page : L<http://alpage.inria.fr>

=item cpanm home page:  L<|http://search.cpan.org/dist/App-cpanminus>

=back


=cut
