<?xml version="1.0"?>
<!-- Author: Isabelle Cabrera "Isabelle.Cabrera@inria.fr" -->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  >

<xsl:output
  method="xml"
  indent="yes"
  encoding="ISO-8859-1"/>

<xsl:template match="*">
  <!-- <xsl:element name="{name()}"> -->
    <xsl:apply-templates select="@*"/>
    <xsl:apply-templates/>
    <!--  </xsl:element> -->
</xsl:template>

<xsl:template match="@*">
  <xsl:copy/>
</xsl:template>

<!-- rename tag pod as software -->
<xsl:template match="/pod">
  <page name="alpidoc" rss="news.rss"> 

<title>
<en>Alpage Installer Documentation</en>
<fr>Documentation d'Alpage Installer</fr>
</title>

<section>
This documentation is generated from alpi pod.
</section>

    <xsl:apply-templates/>

  </page>
</xsl:template>

<!-- head -->

<xsl:template match="head">

</xsl:template>

<!-- renommage de balises -->

<xsl:template match="sect1|sect2|sect3">
  <section><xsl:apply-templates/></section>
</xsl:template>

<xsl:template match="title">
  <title><xsl:apply-templates/></title>
</xsl:template>

<xsl:template match="para">
  <p><xsl:apply-templates/></p>
</xsl:template>

<xsl:template match="verbatim">
  <pre><xsl:apply-templates/></pre>
</xsl:template>

<xsl:template match="list">
  <ul><xsl:apply-templates/></ul>
</xsl:template>

<xsl:template match="emphasis">
  <i><xsl:apply-templates/></i>
</xsl:template>

<xsl:template match="strong">
  <b><xsl:apply-templates/></b>
</xsl:template>

<xsl:template match="link">
  <link href="{@xref}"><xsl:apply-templates/></link>
</xsl:template>

<xsl:template match="item">
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="itemtext">
  <li><xsl:apply-templates/></li>
</xsl:template>

<xsl:template match="filename">
  <tt><xsl:apply-templates/></tt>
</xsl:template>

<xsl:template match="emphasis">
  <tt><xsl:apply-templates/></tt>
</xsl:template>

</xsl:stylesheet>
